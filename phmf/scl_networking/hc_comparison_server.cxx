//
// Created by DannyLo on 04.11.2016.
//

#include <hc_comparison_server.hpp>

/**
@param paillierCryptoProvider the Paillier crypto provider
@param dgkCryptoProvider the DGK crypto provider
@param configurationPath the configuration path for parameters
*/
hc_comparison_server::hc_comparison_server (const Paillier &paillierCryptoProvider,
                                                        const Dgk &dgkCryptoProvider,
                                                        const std::string &configurationPath) :
    SecureComparisonServer(paillierCryptoProvider, dgkCryptoProvider, configurationPath) {
    dgkServer = std::make_shared<hc_dgk_comparison_server>(paillierCryptoProvider, dgkCryptoProvider, configurationPath);
}

void hc_comparison_server::SetClientStubs (hpx::components::client<hc_comparison_client> scClient,
                                             hpx::components::client<hc_dgk_comparison_client> dgkClient) {
    this->scClient = scClient;
    this->dgkClient = dgkClient;
}

/**
@param a encrypted left hand side operand
@param b encrypted right hand side operand
@return @f$ [a] \leq [b] ? [1] : [0] @f$
*/
Paillier::Ciphertext hc_comparison_server::Compare (const Paillier::Ciphertext &a, const Paillier::Ciphertext &b) {
    /// Compute @f$ [z] = [2^l + a - b] = [2^l] [a] [b]^{-1} @f$
    Paillier::Ciphertext z = this->encryptedTwoPowL + a - b;

    /// The MSB of @f$ z @f$, @f$ z_l @f$, is the result of the comparison @f$ (z_l = 0 \Rightarrow a < b)) @f$
    /// To extract @f$ z_l @f$, we need to compute @f$ z_l = 2^{-l} (z - (z \pmod {2^l})) @f$

    /// @f$ (z \pmod {2^l}) @f$ needs to be computed interactively

    /// Additively blind @f$ [z] @f$: @f$ [d] = [z + r] = [z] [r] @f$
    const BlindingFactorContainer &blindingFactorContainer = this->blindingFactorCache.Pop();
    Paillier::Ciphertext d = z + blindingFactorContainer.encryptedR;

    /// Re-randomize [d]
    d = this->paillierCryptoProvider.RandomizeCiphertext(d);//is this really needed?

    /// Ask the client to compute @f$ [-(d \pmod {2^l})] @f$ and encrypted bits of @f$ d @f$
    hpx::future<tuple<Paillier::Ciphertext, std::deque<Dgk::Ciphertext>>> future_tuple = hpx::async<hc_comparison_client::d_calc_action>(scClient, d);
    tuple<Paillier::Ciphertext, std::deque<Dgk::Ciphertext>> minusDModTwoPowLAndHatDBits = future_tuple.get();
    Paillier::Ciphertext minusDModTwoPowL = hpx::util::get<0>(minusDModTwoPowLAndHatDBits);

    /// Since @f$ d \equiv z + r \pmod {2^l} \Rightarrow z \pmod {2^l} = ((d \pmod {2^l}) - (r \pmod {2^l})) \pmod {2^l} @f$
    /// Compute @f$ [-\tilde{z}] = [-(d \pmod {2^l}) + (r \pmod {2^l})] = [-(d \pmod {2^l})] [r \pmod {2^l}] @f$
    Paillier::Ciphertext minusTildeZ = minusDModTwoPowL + blindingFactorContainer.encryptedRModTwoPowL;

    /**
    Because @f$ \tilde{z} @f$ is computed @f$ \pmod n @f$ instead of @f$ \pmod {2^l} @f$, in case of underflows, we need to add @f$ 2^l @f$ to get the right results.
    If @f$ d \pmod {2^l} \geq r \pmod 2^l @f$, @f$ z \pmod {2^l} = \tilde{z} @f$
    Else, an underflow occurs, so we need to add @f$ 2^l @f$ to @f$ \tilde{z} @f$
    We need to compare @f$ \hat{d} = d \pmod {2^l} @f$ (held by the client) and @f$ \hat{r} = r \pmod {2^l} @f$ (held by the server) interactively and store the result in @f$ \lambda @f$
    Once the server obtains @f$ [\lambda] @f$, we can compute @f$ [z \pmod {2^l}] = [\tilde{z} + \lambda 2^l] = [\tilde{z}] [\lambda]^{2^l} @f$
    */

    /// Choose random @f$ s \in {0, 1} @f$
    BigInteger s = RandomProvider::GetInstance().GetRandomInteger(1);

    /// Compute @f$ \lambda @f$
    std::deque<SeComLib::Core::DgkCiphertext> &hatDBits = hpx::util::get<1>(minusDModTwoPowLAndHatDBits);

    Paillier::Ciphertext lambda = dgkServer->ComputeLambda(dgkClient, hatDBits, blindingFactorContainer.hatRBits, s);

    /// @f$ y = z_l = ([z] [-\hat{d}] [\hat{r}] [\lambda])^{2^{-l} \pmod n} @f$ (because @f$ \lambda \in \{0, -2^l\} @f$ instead of @f$ \{-1, 1\} @f$ - see protocol 4.11)
    Paillier::Ciphertext y = (z + minusDModTwoPowL + blindingFactorContainer.encryptedRModTwoPowL + lambda) * twoPowMinusLModN;

    return y;
}