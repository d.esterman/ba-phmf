//
// Created by DannyLo on 03.11.2016.
//

#include "hc_dgk_comparison_client.hpp"

hc_dgk_comparison_client::hc_dgk_comparison_client() :
        DgkComparisonClient(Paillier(), Dgk(), "") { }

hc_dgk_comparison_client::hc_dgk_comparison_client(const Paillier &paillierCryptoProvider, const Dgk &dgkCryptoProvider,
                                                 const std::string &configurationPath) :
        DgkComparisonClient(paillierCryptoProvider, dgkCryptoProvider, configurationPath) {}

std::deque<Dgk::Ciphertext> hc_dgk_comparison_client::GetHatDBits(const BigInteger &hatD) const {
    std::deque<Dgk::Ciphertext> hatDBits;
    /// Encrypt each bit of @f$ \hat{d} @f$ using DGK @f$ \Rightarrow \llbracket \hat{d} \rrbracket @f$
    for (size_t i = 0; i < this->l; ++i) {
        hatDBits.emplace_back(this->dgkCryptoProvider.EncryptInteger(BigInteger(static_cast<long>(hatD.GetBit(i)))));
    }

    return hatDBits;
}

Paillier::Ciphertext hc_dgk_comparison_client::ComputeLambda (const std::deque<Dgk::Ciphertext> &e) const {
    return DgkComparisonClient::ComputeLambda(e);
}

HPX_REGISTER_COMPONENT(hpx::components::component<hc_dgk_comparison_client>, hc_dgk_comparison_client);

HPX_REGISTER_ACTION(
        hpx::components::component<hc_dgk_comparison_client>::wrapped_type::lambda_calc_action,
        dgk_lambda_calc_action);