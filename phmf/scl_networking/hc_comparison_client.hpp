//
// Created by DannyLo on 03.11.2016.
//

#ifndef BA_PHMF_CONCURRENTCOMPARISONCLIENT_HPP
#define BA_PHMF_CONCURRENTCOMPARISONCLIENT_HPP

// HPX
#include <hpx/include/components.hpp>
#include <hpx/util/tuple.hpp>

// SeComLib
#include <secure_comparison_client.h>
#include <big_integer.h>
#include <paillier.h>
#include <dgk.h>

// Own headers
#include <export_definitions.hpp>
#include <serialization.hpp>
#include <hc_dgk_comparison_client.hpp>

using SeComLib::SecureFaceRecognitionUtils::SecureComparisonClient;
using SeComLib::Core::Paillier;
using SeComLib::Core::Dgk;
using SeComLib::Core::BigInteger;
using hpx::util::tuple;
using hpx::util::make_tuple;

class PHMF_SYMBOL_EXPORT hc_comparison_client : public SecureComparisonClient,
                                   public hpx::components::locking_hook<
                                           hpx::components::component_base<hc_comparison_client> > {

    /// A reference to the DgkComparisonClient
    std::shared_ptr<hc_dgk_comparison_client> dgkComparisonClient;

public:
    // Default constructor
    hc_comparison_client ();

    /// Constructor
    hc_comparison_client (const Paillier &paillierCryptoProvider, const Dgk &dgkCryptoProvider, const std::string &configurationPath);

    /// Computes @f$ [-(d \pmod {2^l})] @f$ and bitwise @f$ [\hat{d}] @f$
    tuple<Paillier::Ciphertext, std::deque<Dgk::Ciphertext>> ComputeMinusDModTwoPowLAndHatDBits (const Paillier::Ciphertext &d) const;

    HPX_DEFINE_COMPONENT_ACTION(hc_comparison_client, ComputeMinusDModTwoPowLAndHatDBits, d_calc_action);
};

HPX_REGISTER_ACTION_DECLARATION(
        hc_comparison_client::d_calc_action,
        scc_d_calc_action);


#endif //BA_PHMF_CONCURRENTCOMPARISONCLIENT_HPP
