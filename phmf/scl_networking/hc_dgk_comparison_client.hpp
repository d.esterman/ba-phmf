//
// Created by DannyLo on 03.11.2016.
//

#ifndef BA_PHMF_CONCURRENTDGKCOMPCLIENT_HPP
#define BA_PHMF_CONCURRENTDGKCOMPCLIENT_HPP

// HPX
#include <hpx/include/components.hpp>

// SeComLib
#include <dgk_comparison_client.h>
#include <big_integer.h>
#include <paillier.h>
#include <dgk.h>

// Own headers
#include <export_definitions.hpp>
#include <serialization.hpp>


using SeComLib::SecureFaceRecognitionUtils::DgkComparisonClient;
using SeComLib::Core::Paillier;
using SeComLib::Core::Dgk;
using SeComLib::Core::BigInteger;

using std::unordered_map;

class PHMF_SYMBOL_EXPORT hc_dgk_comparison_client:public DgkComparisonClient,
                              public hpx::components::locking_hook<
                                      hpx::components::component_base<hc_dgk_comparison_client> > {

public:
    /// Default constructor
    hc_dgk_comparison_client ();

    /// Constructor
    hc_dgk_comparison_client (const Paillier &paillierCryptoProvider, const Dgk &dgkCryptoProvider, const std::string &configurationPath);

    /// Computes the encrypted bits of @f$ \hat{d} @f$ for a specific thread
    std::deque<Dgk::Ciphertext> GetHatDBits (const BigInteger &hatD) const;

    Paillier::Ciphertext ComputeLambda (const std::deque<Dgk::Ciphertext> &e) const;

    HPX_DEFINE_COMPONENT_ACTION(hc_dgk_comparison_client, ComputeLambda, lambda_calc_action);
};

HPX_REGISTER_ACTION_DECLARATION(
        hc_dgk_comparison_client::lambda_calc_action,
        dgk_lambda_calc_action);


#endif //BA_PHMF_CONCURRENTDGKCOMPCLIENT_HPP
