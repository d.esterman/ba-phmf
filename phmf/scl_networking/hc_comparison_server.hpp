//
// Created by DannyLo on 04.11.2016.
//

#ifndef BA_PHMF_CONCURRENTSECURECOMPSERVER_HPP
#define BA_PHMF_CONCURRENTSECURECOMPSERVER_HPP

// HPX
#include <hpx/hpx.hpp>
#include <hpx/util/tuple.hpp>

// SeComLib
#include <secure_comparison_server.h>
#include <paillier.h>
#include <dgk.h>
#include <big_integer.h>

// Own headers
#include <export_definitions.hpp>
#include <hc_comparison_client.hpp>
#include <hc_dgk_comparison_server.hpp>

using SeComLib::SecureFaceRecognitionUtils::SecureComparisonServer;
using SeComLib::SecureFaceRecognitionUtils::DgkComparisonServer;
using SeComLib::Core::Paillier;
using SeComLib::Core::Dgk;
using SeComLib::Core::BigInteger;
using SeComLib::Core::RandomProvider;
using hpx::util::tuple;


class PHMF_SYMBOL_EXPORT hc_comparison_server : public SecureComparisonServer {

    hpx::components::client<hc_comparison_client> scClient;
    hpx::components::client<hc_dgk_comparison_client> dgkClient;

    std::shared_ptr<hc_dgk_comparison_server> dgkServer;

public:
    /// Constructor
    hc_comparison_server(const Paillier &paillierCryptoProvider,
                               const Dgk &dgkCryptoProvider,
                               const std::string &configurationPath);

    /// Interactive secure comparison
    Paillier::Ciphertext Compare(const Paillier::Ciphertext &a, const Paillier::Ciphertext &b);

    /// Setter for client stubs
    void SetClientStubs(hpx::components::client<hc_comparison_client> scClient,
                        hpx::components::client<hc_dgk_comparison_client> dgkClient);
};

#endif //BA_PHMF_CONCURRENTSECURECOMPSERVER_HPP
