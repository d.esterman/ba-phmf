//
// Created by DannyLo on 04.11.2016.
//

#ifndef BA_PHMF_CONCURRENTDGKCOMPSERVER_HPP
#define BA_PHMF_CONCURRENTDGKCOMPSERVER_HPP

#include <dgk_comparison_server.h>
#include <dgk_comparison_client.h>

using SeComLib::SecureFaceRecognitionUtils::DgkComparisonServer;
using SeComLib::Core::Paillier;
using SeComLib::Core::BigInteger;
using SeComLib::Core::Dgk;
using SeComLib::Core::SecurePermutation;

class hc_dgk_comparison_server:public DgkComparisonServer {
public:
    /// Constructor
    hc_dgk_comparison_server (const Paillier &paillierCryptoProvider, const Dgk &dgkCryptoProvider, const std::string &configurationPath);

    /// Compute @f$ \lambda @f$
    Paillier::Ciphertext ComputeLambda (hpx::components::client<hc_dgk_comparison_client> dgkClient,
                                        const std::deque<Dgk::Ciphertext> &hatDBits,
                                        const std::deque<long> &hatRBits, const BigInteger &s);
};


#endif //BA_PHMF_CONCURRENTDGKCOMPSERVER_HPP
