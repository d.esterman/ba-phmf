//
// Created by DannyLo on 03.11.2016.
//

#include <hc_comparison_client.hpp>
hc_comparison_client::hc_comparison_client () :
        SecureComparisonClient(Paillier(), Dgk(), "") {}
/**
	@param paillierCryptoProvider the Paillier crypto provider
	@param dgkCryptoProvider the Dgk crypto provider
	@param configurationPath the configuration path for parameters
	*/
hc_comparison_client::hc_comparison_client (const Paillier &paillierCryptoProvider, const Dgk &dgkCryptoProvider, const std::string &configurationPath) :
        SecureComparisonClient(paillierCryptoProvider, dgkCryptoProvider, configurationPath) {
    dgkComparisonClient = std::make_shared<hc_dgk_comparison_client>(paillierCryptoProvider, dgkCryptoProvider, configurationPath);
}

/**
@param d @f$ [d] @f$
@return A tuple with @f$ [-(d \pmod {2^l})] @f$ and
*/
tuple<Paillier::Ciphertext, std::deque<Dgk::Ciphertext>> hc_comparison_client::ComputeMinusDModTwoPowLAndHatDBits (const Paillier::Ciphertext &encryptedD) const {
    const BigInteger &d = this->paillierCryptoProvider.DecryptInteger(encryptedD);
    BigInteger plaintextDModTwoPowL = d % this->twoPowL;

    /// Calculate the encryption of each bit of @f$ \hat{d} @f$
    std::deque<SeComLib::DgkCiphertext> hatDBits = dgkComparisonClient->GetHatDBits(d);

    /// Return a tuple consisting of @f$ [-(d \pmod {2^l})] @f$ and encrypted bits of @f$ d @f$
    return hpx::util::make_tuple(this->paillierCryptoProvider.EncryptInteger(-plaintextDModTwoPowL), hatDBits);
}

HPX_REGISTER_COMPONENT_MODULE();

HPX_REGISTER_COMPONENT(hpx::components::component<hc_comparison_client>, hc_comparison_client);

HPX_REGISTER_ACTION(
        hpx::components::component<hc_comparison_client>::wrapped_type::d_calc_action,
        scc_d_calc_action);