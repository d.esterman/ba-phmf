//
// Created by DannyLo on 27.10.2016.
//

#include <hpx/hpx_init.hpp>
#include <bz_server.hpp>


int main(int argc, char* argv[])
{
    return hpx::init(argc, argv);
}

int hpx_main() {
    hpx::new_<bz_server>(hpx::find_here());
    hpx::agas::register_name("phmf_server", hpx::find_here());
}