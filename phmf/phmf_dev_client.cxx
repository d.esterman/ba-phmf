//
// Created by DannyLo on 27.10.2016.
//

#include <hpx/hpx_init.hpp>
#include <bz_client.hpp>
#include <file_fingerprint_provider.hpp>
#include <hpx_zfm_fingerprint_provider.hpp>


int main(int argc, char* argv[])
{
    using boost::program_options::options_description;
    using boost::program_options::value;

    // Configure application-specific options
    options_description
            desc_commandline("Usage: " HPX_APPLICATION_STRING " [options]");

    desc_commandline.add_options()
            ( "filename,f"
                    , value<std::string>()
                    , "A filename to read a fingerprint from (bmp or jpg)");
    return hpx::init(desc_commandline, argc, argv);
}

int hpx_main(boost::program_options::variables_map& vm) {
    std::unique_ptr<fingerprint_provider> fp;
    bool loop = true;
    if (vm.count("filename")) {
        std::string filename = vm["filename"].as<std::string>();
        fp = std::make_unique<file_fingerprint_provider>(file_fingerprint_provider(filename));
        loop = false;
    }
    else {
        hpx::future<hpx::id_type> f = hpx::agas::on_symbol_namespace_event("phmf_zfm_server", true);
        fp = std::make_unique<hpx_zfm_fingerprint_provider>(hpx::components::new_<hpx_zfm_fingerprint_provider_server>(f.get()));
    }
    bz_client c(hpx::components::local_new<bz_server>(), *fp);
    if (loop) {
        while (true) {
            std::cout << (c.authenticate() ? "Authenticated" : "Not authenticated") << std::endl;
        }
    }
	
	std::cout << (c.authenticate() ? "Authenticated" : "Not authenticated") << std::endl;
    return hpx::finalize();
}
