//
// Created by DannyLo on 09.05.2017.
//

#ifndef BA_PHMF_EXPORT_DEFINITIONS_HPP
#define BA_PHMF_EXPORT_DEFINITIONS_HPP

#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
# ifdef PHMF_EXPORTS
#  define PHMF_SYMBOL_EXPORT      __declspec(dllexport)
# else
#  define PHMF_SYMBOL_EXPORT      __declspec(dllimport)
# endif
#else
# define PHMF_SYMBOL_EXPORT      /* empty */
#endif

#endif //BA_PHMF_EXPORT_DEFINITIONS_HPP
