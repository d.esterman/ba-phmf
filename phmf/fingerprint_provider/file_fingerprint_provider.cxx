//
// Created by DannyLo on 14.05.2017.
//

#include <file_fingerprint_provider.hpp>

file_fingerprint_provider::file_fingerprint_provider(std::string file_name) :
        file_name(file_name) {}

std::shared_ptr<std::tuple<const int **, int>> file_fingerprint_provider::get_fingerprint() const {
    std::string bmpsuffix = ".bmp";
    std::string jpgsuffix = ".jpg";
    std::string xytsuffix = ".xyt";
    if (file_name.size() >= xytsuffix.size() &&
            file_name.compare(file_name.size() - xytsuffix.size(), xytsuffix.size(), xytsuffix) == 0) {
        FILE *xytfile;
        xytfile = fopen(const_cast<char *>(file_name.c_str()), "r" );
        if (xytfile == (FILE *) NULL )
        {
            fprintf( errorfp, "ERROR: fopen() of minutiae file \"%s\" failed: %s\n",
                     const_cast<char *>(file_name.c_str()), strerror(errno) );
            return nullptr;
        }
        xyt_struct* xyt = _bz_load(xytfile);
        auto ret = compute_itrfc_from_xyt(xyt);
        fclose(xytfile);
        return ret;
    } else if (file_name.size() >= bmpsuffix.size() &&
            file_name.compare(file_name.size() - bmpsuffix.size(), bmpsuffix.size(), bmpsuffix) == 0) {
        FILE *bmpfile;
        if ((bmpfile = fopen(const_cast<char *>(file_name.c_str()), "rb")) == (FILE *) NULL) {
            fprintf(stderr, "ERORR : get_fingerprint : fopen : %s\n", bmpfile);
            return nullptr;
        }
        auto ret = compute_itrfc_from_bmp_file(bmpfile);
        fclose(bmpfile);
        return ret;
    } else if (file_name.compare(file_name.size() - jpgsuffix.size(), jpgsuffix.size(), jpgsuffix) == 0) {
        FILE *jpgfile;
        if ((jpgfile = fopen(const_cast<char *>(file_name.c_str()), "rb")) == (FILE *) NULL) {
            fprintf(stderr, "ERORR : get_fingerprint : fopen : %s\n", jpgfile);
            return nullptr;
        }
        auto ret = compute_itafc_from_jpg_file(jpgfile);
        fclose(jpgfile);
        return ret;
    }
    throw std::runtime_error("The fingerprint file format is unknown.");
}