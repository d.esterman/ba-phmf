//
// Created by DannyLo on 14.05.2017.
//

#ifndef BA_PHMF_FILE_FINGERPRINT_PROVIDER_HPP
#define BA_PHMF_FILE_FINGERPRINT_PROVIDER_HPP

#include <string>

#include <fingerprint_provider.hpp>

class PHMF_SYMBOL_EXPORT file_fingerprint_provider : public fingerprint_provider {
    const std::string file_name;
public:
    file_fingerprint_provider(std::string file_name);
    std::shared_ptr<std::tuple<const int**, int>> get_fingerprint() const;
};


#endif //BA_PHMF_FILE_FINGERPRINT_PROVIDER_HPP
