//
// Created by DannyLo on 14.05.2017.
//

#ifndef BA_PHMF_FINGERPRINT_PROVIDER_HPP
#define BA_PHMF_FINGERPRINT_PROVIDER_HPP

#include <tuple>
#include <memory>

#include <export_definitions.hpp>
#include <hd_bz3.hpp>
extern "C" {
#include <bz_mindtct_util.h>
};

class fingerprint_provider {
public:
    virtual std::shared_ptr<std::tuple<const int**, int>> get_fingerprint() const = 0;
    std::shared_ptr<std::tuple<const int **, int>> compute_itrfc_from_xyt(xyt_struct *xyt) const;
    std::shared_ptr<std::tuple<const int **, int>> compute_itrfc_from_bmp_file(FILE *bmpfile) const;
    std::shared_ptr<std::tuple<const int **, int>> compute_itafc_from_jpg_file(FILE *jpgfile) const;
};

#endif //BA_PHMF_FINGERPRINT_PROVIDER_HPP
