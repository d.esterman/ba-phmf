//
// Created by DannyLo on 14.05.2017.
//

#ifndef BA_PHMF_HPX_ZFM_FINGERPRINT_PROVIDER_HPP
#define BA_PHMF_HPX_ZFM_FINGERPRINT_PROVIDER_HPP

#include <hpx/hpx.hpp>

#include <fingerprint_provider.hpp>
#include <hpx_zfm_fingerprint_provider_server.hpp>

class PHMF_SYMBOL_EXPORT hpx_zfm_fingerprint_provider : public fingerprint_provider,
                                                        public hpx::components::client_base<
                                                                hpx_zfm_fingerprint_provider_server, hpx_zfm_fingerprint_provider_server> {

    typedef hpx::components::client_base<hpx_zfm_fingerprint_provider_server, hpx_zfm_fingerprint_provider_server> base_type;

public:
    hpx_zfm_fingerprint_provider(hpx::future<hpx::id_type> &&gid);
    std::shared_ptr<std::tuple<const int**, int>> get_fingerprint() const;
};


#endif //BA_PHMF_HPX_ZFM_FINGERPRINT_PROVIDER_HPP
