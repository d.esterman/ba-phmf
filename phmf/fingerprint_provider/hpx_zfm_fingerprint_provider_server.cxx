//
// Created by DannyLo on 15.05.2017.
//

#include <hpx_zfm_fingerprint_provider_server.hpp>

extern "C" {
#include <zfm20.h>
}

std::vector<char> hpx_zfm_fingerprint_provider_server::get_fingerprint_from_zfm20() {
    char buffer[bmp_file_size];
    std::vector<char> wrapped_buffer(bmp_file_size);
    if(read_buffer_from_zfm(buffer))
    {
        return wrapped_buffer;
    }
    std::copy_n(buffer, bmp_file_size, wrapped_buffer.begin());
    return wrapped_buffer;
};

int hpx_zfm_fingerprint_provider_server::read_buffer_from_zfm(char* buffer)
{
    int fd;
    if ((fd  = begin(115200)) < 0) {
        fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
        return 1;
    }

    char p = -1;
    if (verifyPassword(fd))
        printf("Found fingerprint sensor!\n");
    else {
        printf("Could not find the fingerprint sensor :(\n");
        return 1;
    }

    //Force STDOUT to be unbufered
    setvbuf(stdout, NULL, _IONBF, 0);

    printf("Waiting for valid finger to enroll\n");
    while (p != FINGERPRINT_OK) {
        p = getImage(fd);
        switch (p) {
            case FINGERPRINT_OK:
                printf("\nImage taken\n");
                break;
            case FINGERPRINT_NOFINGER:
                printf(".");
                break;
            case FINGERPRINT_PACKETRECIEVEERR:
                printf("Communication error\n");
                return 1;
            case FINGERPRINT_IMAGEFAIL:
                printf("Imaging error\n");
                return 1;
            default:
                printf("Unknown error\n");
                return 1;
        }
    }
    printf("\n");

    //Turn the buffer on again
    setvbuf(stdout, NULL, _IOLBF, 0);

    return getImageBuffer(fd, buffer);
}

HPX_REGISTER_COMPONENT(hpx::components::component<hpx_zfm_fingerprint_provider_server>, hpx_zfm_fingerprint_provider_server)

HPX_REGISTER_ACTION(hpx::components::component<hpx_zfm_fingerprint_provider_server>::wrapped_type::get_fingerprint_from_zfm20_action,
                    fingerprint_provider_server_get_fingerprint_from_zfm20_action)