//
// Created by DannyLo on 15.05.2017.
//

#include <fingerprint_provider.hpp>

using std::tuple;
using std::shared_ptr;
using std::make_tuple;
using std::make_shared;

shared_ptr<tuple<const int **, int>> fingerprint_provider::compute_itrfc_from_bmp_file(FILE *bmpfile) const {
    FILE *jpgfile = tmpfile();
    convert_bmp_to_jpeg(bmpfile, jpgfile);
    auto ret = compute_itafc_from_jpg_file(jpgfile);
    fclose(jpgfile);
    return ret;
};

shared_ptr<tuple<const int **, int>> fingerprint_provider::compute_itafc_from_jpg_file(FILE *jpgfile) const {
    struct xytq_struct probe_q;             //input probe fingerprint including quality
    struct xyt_struct *probe;               //processed fp w/o quality and with corrected angles

    MINUTIAE *minutiae;
    int ih;

    mindtct(jpgfile, 1, &minutiae, &ih);

    probe_q.nrows = minutiae->num;
    int i, ox, oy, ot, oq;
    for (i = 0; i < minutiae->num; i++) {
        MINUTIA *minutia = minutiae->list[i];
        lfs2nist_minutia_XYT(&ox, &oy, &ot, minutia, 0 /* not used */, ih);
        oq = sround(minutia->reliability * 100.0);

        probe_q.xcol[i] = ox;
        probe_q.ycol[i] = oy;
        probe_q.thetacol[i] = ot;
        probe_q.qualitycol[i] = oq;
    }

    free_minutiae(minutiae);
    probe = bz_prune(&probe_q, 0); // bz_io.c:460
    return compute_itrfc_from_xyt(probe);
}


shared_ptr<tuple<const int **, int>> fingerprint_provider::compute_itrfc_from_xyt(xyt_struct *xyt) const {
    int p_itafc[SCOLS_SIZE_1][COLS_SIZE_2]; //itafc for the probe
    int *p_itafc_ptrs[SCOLPT_SIZE];        //pointers to p_itafc
    int probe_len = bozorth_itafc_init(xyt, p_itafc, p_itafc_ptrs);
    return make_shared<tuple<const int **, int>>(
            make_tuple(const_cast<const int **>(p_itafc_ptrs), probe_len));
};