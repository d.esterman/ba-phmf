//
// Created by DannyLo on 15.05.2017.
//

#ifndef BA_PHMF_HPX_ZFM_FINGERPRINT_PROVIDER_SERVER_HPP
#define BA_PHMF_HPX_ZFM_FINGERPRINT_PROVIDER_SERVER_HPP

#include <hpx/hpx.hpp>

#include <export_definitions.hpp>

extern "C" {
#include <bmpheader.h>
};

class PHMF_SYMBOL_EXPORT hpx_zfm_fingerprint_provider_server : public hpx::components::locking_hook<
        hpx::components::component_base<hpx_zfm_fingerprint_provider_server> > {
    int read_buffer_from_zfm(char* buffer);
public:
    std::vector<char> get_fingerprint_from_zfm20();

    HPX_DEFINE_COMPONENT_ACTION(hpx_zfm_fingerprint_provider_server, get_fingerprint_from_zfm20);
};

HPX_REGISTER_ACTION_DECLARATION(hpx_zfm_fingerprint_provider_server::get_fingerprint_from_zfm20_action,
                                fingerprint_provider_server_get_fingerprint_from_zfm20_action);

#endif //BA_PHMF_HPX_ZFM_FINGERPRINT_PROVIDER_SERVER_HPP
