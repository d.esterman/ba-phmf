//
// Created by DannyLo on 14.05.2017.
//

#include <hpx_zfm_fingerprint_provider.hpp>
#include <hpx/hpx_init.hpp>

//dummy declaration to satisfy the linker
#ifndef __arm__
std::vector<char> hpx_zfm_fingerprint_provider_server::get_fingerprint_from_zfm20() { };

HPX_REGISTER_COMPONENT(hpx::components::component<hpx_zfm_fingerprint_provider_server>, hpx_zfm_fingerprint_provider_server)

HPX_REGISTER_ACTION(hpx::components::component<hpx_zfm_fingerprint_provider_server>::wrapped_type::get_fingerprint_from_zfm20_action,
                    fingerprint_provider_server_get_fingerprint_from_zfm20_action)
#endif

hpx_zfm_fingerprint_provider::hpx_zfm_fingerprint_provider(hpx::future<hpx::id_type> &&gid) :
        base_type(std::move(gid)) {}

std::shared_ptr<std::tuple<const int **, int>> hpx_zfm_fingerprint_provider::get_fingerprint() const {
    FILE *bmpfile = tmpfile();
    typedef hpx_zfm_fingerprint_provider_server::get_fingerprint_from_zfm20_action action_type;
    std::vector<char> buffer = action_type()(this->get_id());
    if (buffer[0] != 'B') {
        std::cerr << "ERROR: bitmap from zfm20 corrupt" << std::endl;
        hpx::finalize();
    }
    fwrite(&buffer[0], sizeof(std::vector<char>::value_type), buffer.size(), bmpfile);
    rewind(bmpfile);
    auto ret = compute_itrfc_from_bmp_file(bmpfile);
    fclose(bmpfile);
    return ret;
}

