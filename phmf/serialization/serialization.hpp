//
// Created by DannyLo on 27.04.2017.
//
#ifndef SERIALIZATION_HPP
#define SERIALIZATION_HPP

// STL
#include <string>
#include <iostream>

// SeComLib
#include <big_integer.h>
#include <paillier.h>
#include <dgk.h>

// HPX
#include <hpx/runtime/serialization/shared_ptr.hpp>
#include <hpx/runtime/serialization/deque.hpp>
#include <hpx/runtime/serialization/vector.hpp>

namespace SeComLib {
    namespace Core {
        template <class Archive>
        void serialize(Archive & ar, BigInteger &b, const unsigned int version)
        {
            ar & b.data;
        }

        template<class Archive>
        void serialize(Archive &ar, PaillierPublicKey &p, const unsigned int version) {
            ar & p.n;
            ar & p.g;
        }

        template<class Archive>
        void serialize(Archive &ar, DgkPublicKey &d, const unsigned int version) {
            ar & d.n;
            ar & d.g;
            ar & d.h;
            ar & d.u;
        }

        template<class Archive, typename T_CiphertextImpl>
        void serialize(Archive &ar, CiphertextBase<T_CiphertextImpl>& c, const unsigned int version) {
            ar & c.data;
            ar & c.encryptionModulus;
        }
    }
}



template<class Archive>
void load(Archive &ar, __mpz_struct &mpz, const unsigned int version) {
    uint32_t count;
    ar & count;
    if (count > std::numeric_limits<size_t>::max()) {
        throw std::runtime_error("Error at deserializing mpz. "
		                         "Received integers are not 32 bit!");
    }
	char* buf = new char[count];
    for (int i = 0; i < count; i++) {
        ar & buf[i];
    }
	mpz_import (&mpz, count, 1, sizeof(char), 1, 0, buf);
	delete[] buf;
}

template<class Archive>
void save(Archive &ar, const __mpz_struct &mpz, const unsigned int version) {
    size_t count;
    char* to_send = (char*)mpz_export (nullptr, &count, 1,
                                       sizeof(char), 1, 0, &mpz);
    ar & (uint32_t)count;
    for (int i = 0; i < count; i++) {
        ar & to_send[i];
    }
    void (*freefunc) (void *, size_t);
    mp_get_memory_functions (NULL, NULL, &freefunc);
    freefunc(to_send, count);
}

HPX_SERIALIZATION_SPLIT_FREE(__mpz_struct)

#undef uint

#endif