//
// Created by DannyLo on 08.11.2016.
//
#include <hpx/runtime/serialization/input_archive.hpp>
#include <hpx/runtime/serialization/output_archive.hpp>
#include <hpx/util/tuple.hpp>

#include <gtest/gtest.h>

#include <fstream>


#include <serialization.hpp>

using std::vector;
using std::array;
using hpx::util::tuple;
using std::deque;

using SeComLib::Core::Paillier;
using SeComLib::Core::PaillierPublicKey;
using SeComLib::Core::Dgk;
using SeComLib::Core::DgkPublicKey;
using SeComLib::Core::DgkCiphertext;
using SeComLib::Core::PaillierCiphertext;
using SeComLib::Core::BigInteger;

TEST(serializaion, ppk) {
    Paillier p;
    p.GenerateKeys();
    const PaillierPublicKey pk = p.GetPublicKey();
    std::vector<char> buffer;
    {
        hpx::serialization::output_archive oa(buffer);
        oa << pk;
    }
    PaillierPublicKey new_pk;
    {
        hpx::serialization::input_archive ia(buffer);
        ia >> new_pk;
    }

    EXPECT_EQ(pk.g, new_pk.g);
    EXPECT_EQ(pk.n, new_pk.n);
}

TEST(serializaion, dkp) {
    Dgk dgk;
    dgk.GenerateKeys();
    const DgkPublicKey pk = dgk.GetPublicKey();
    std::vector<char> buffer;
    {
        hpx::serialization::output_archive oa(buffer);
        oa << pk;
    }
    DgkPublicKey new_pk;
    {
        hpx::serialization::input_archive ia(buffer);
        ia >> new_pk;
    }
    EXPECT_EQ(pk.g, new_pk.g);
    EXPECT_EQ(pk.n, new_pk.n);
    EXPECT_EQ(pk.h, new_pk.h);
    EXPECT_EQ(pk.u, new_pk.u);
}

TEST(serializaion, dTuple) {
    std::vector<char> buffer;
    Paillier p; Dgk dgk; p.GenerateKeys(); dgk.GenerateKeys();
    deque<DgkCiphertext> hatDBits { dgk.EncryptInteger(BigInteger(2020)), dgk.EncryptInteger(BigInteger(-4)) };
    tuple<PaillierCiphertext, std::deque<DgkCiphertext>> dTuple = hpx::util::make_tuple(p.EncryptInteger(BigInteger(1321)), hatDBits);
    {
        hpx::serialization::output_archive oa(buffer);
        oa << dTuple;
    }
    tuple<PaillierCiphertext, std::deque<DgkCiphertext>> new_dTuple;
    {
        hpx::serialization::input_archive ia(buffer);
        ia >> new_dTuple;
    }

    EXPECT_EQ(hpx::util::get<0>(dTuple).data, hpx::util::get<0>(new_dTuple).data);

    deque<DgkCiphertext> &new_hatDBits = hpx::util::get<1>(dTuple);
    for (unsigned int i = 0; i<new_hatDBits.size(); i++) {
        EXPECT_EQ(hatDBits[i].data, new_hatDBits[i].data);
    }
}

TEST(serializaion, itaFC) {
    Paillier p; p.GenerateKeys();
    std::vector<char> buffer;
    vector<array<PaillierCiphertext, 6>> itaFC{array<PaillierCiphertext, 6> {
            p.EncryptInteger(BigInteger(111)),
            p.EncryptInteger(BigInteger(211)),
            p.EncryptInteger(BigInteger(311)),
            p.EncryptInteger(BigInteger(411)),
            p.EncryptInteger(BigInteger(511)),
            p.EncryptInteger(BigInteger(-611)),
    }};
    {
        hpx::serialization::output_archive oa(buffer);
        oa << itaFC;
    }
    vector<array<PaillierCiphertext, 6>> new_itaFC;
    {
        hpx::serialization::input_archive ia(buffer);
        ia >> new_itaFC;
    }

    EXPECT_EQ(new_itaFC[0][0].data, itaFC[0][0].data);
    EXPECT_EQ(new_itaFC[0][0].encryptionModulus.get()[0], itaFC[0][0].encryptionModulus.get()[0]);
    EXPECT_EQ(new_itaFC[0][1].data, itaFC[0][1].data);
    EXPECT_EQ(new_itaFC[0][1].encryptionModulus.get()[0], itaFC[0][1].encryptionModulus.get()[0]);
    EXPECT_EQ(new_itaFC[0][2].data, itaFC[0][2].data);
    EXPECT_EQ(new_itaFC[0][2].encryptionModulus.get()[0], itaFC[0][2].encryptionModulus.get()[0]);
    EXPECT_EQ(new_itaFC[0][3].data, itaFC[0][3].data);
    EXPECT_EQ(new_itaFC[0][3].encryptionModulus.get()[0], itaFC[0][3].encryptionModulus.get()[0]);
    EXPECT_EQ(new_itaFC[0][4].data, itaFC[0][4].data);
    EXPECT_EQ(new_itaFC[0][4].encryptionModulus.get()[0], itaFC[0][4].encryptionModulus.get()[0]);
    EXPECT_EQ(new_itaFC[0][5].data, itaFC[0][5].data);
    EXPECT_EQ(new_itaFC[0][5].encryptionModulus.get()[0], itaFC[0][5].encryptionModulus.get()[0]);
}
