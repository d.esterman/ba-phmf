//
// Created by DannyLo on 27.10.2016.
//

#include <hpx/hpx_init.hpp>
#include <bz_client.hpp>
#include <file_fingerprint_provider.hpp>


int main(int argc, char* argv[])
{
    using boost::program_options::options_description;
    using boost::program_options::value;

    // Configure application-specific options
    options_description
            desc_commandline("Usage: " HPX_APPLICATION_STRING " [options]");

    desc_commandline.add_options()
            ( "filename,f"
                    , value<std::string>()->required()
                    , "A filename to read a fingerprint from (bmp, jpg or xyt)");
    return hpx::init(desc_commandline, argc, argv);
}

int hpx_main(boost::program_options::variables_map& vm) {
    std::string filename = vm["filename"].as<std::string>();
    file_fingerprint_provider fp(filename);
    bz_client c(hpx::components::local_new<bz_server>(), fp);
    std::cout << (c.authenticate() ? "Authenticated" : "Not authenticated") << std::endl;
    return hpx::finalize();
}
