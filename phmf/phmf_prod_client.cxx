//
// Created by DannyLo on 27.10.2016.
//

#include <hpx/hpx_init.hpp>
#include <bz_client.hpp>
#include <file_fingerprint_provider.hpp>
#include <hpx_zfm_fingerprint_provider.hpp>


int main(int argc, char* argv[])
{
    using boost::program_options::options_description;
    using boost::program_options::value;

    // Configure application-specific options
    options_description
            desc_commandline("Usage: " HPX_APPLICATION_STRING " [options]");

    desc_commandline.add_options()
            ( "filename,f"
                    , value<std::string>()
                    , "A filename to read a fingerprint from (bmp, jpg or xyt)");
    return hpx::init(desc_commandline, argc, argv);
}

int hpx_main(boost::program_options::variables_map& vm) {
    hpx::future<hpx::id_type> f = hpx::agas::on_symbol_namespace_event("phmf_server", true);
    std::unique_ptr<fingerprint_provider> fp;
    bool loop = true;
    if (vm.count("filename")) {
        std::string filename = vm["filename"].as<std::string>();
        fp = std::make_unique<file_fingerprint_provider>(file_fingerprint_provider(filename));
        loop = false;
    }
    else {
        fp = std::make_unique<hpx_zfm_fingerprint_provider>(hpx::components::local_new<hpx_zfm_fingerprint_provider_server>());
    }
    hpx::future<hpx::id_type> s = hpx::agas::on_symbol_namespace_event("phmf_server", true);
    bz_client c(hpx::components::new_<bz_server>(s.get()), *fp);
    if (loop) {
        while (true) {
            std::cout << (c.authenticate() ? "Authenticated" : "Not authenticated") << std::endl;
        }
    }
	
    std::cout << (c.authenticate() ? "Authenticated" : "Not authenticated") << std::endl;
    return hpx::finalize();
}
