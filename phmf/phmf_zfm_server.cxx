//
// Created by DannyLo on 27.10.2016.
//

#include <hpx/hpx_init.hpp>
#include <bz_server.hpp>
#include <hpx_zfm_fingerprint_provider_server.hpp>


int main(int argc, char* argv[])
{
    return hpx::init(argc, argv);
}

int hpx_main() {
    hpx::new_<hpx_zfm_fingerprint_provider_server>(hpx::find_here());
    hpx::agas::register_name("phmf_zfm_server", hpx::find_here());
}