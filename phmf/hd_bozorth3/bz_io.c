#include <string.h>
#include <hd_bz3.h>


/************************************************************************
Load a XYTQ structure and return a XYT struct. 
Row 3's value is an angle which is normalized to the interval (-180,180].
A maximum of MAX_BOZORTH_MINUTIAE minutiae can be returned -- fewer if
"max_minutiae" is smaller.  If the file contains more minutiae than are
to be returned, the highest-quality minutiae are returned.
*************************************************************************/
struct xyt_struct * bz_prune(struct xytq_struct *xytq_s, int verbose_load)
{

   int nminutiae;
   int j;
   struct xyt_struct * xyt_s;
   int * xptr;
   int * yptr;
   int * tptr;
   int * qptr;
   struct minutiae_struct c[MAX_FILE_MINUTIAE];
   int xvals_lng[MAX_FILE_MINUTIAE],
       yvals_lng[MAX_FILE_MINUTIAE],
       tvals_lng[MAX_FILE_MINUTIAE],
       qvals_lng[MAX_FILE_MINUTIAE];
   int order[MAX_FILE_MINUTIAE];       
   int xvals[MAX_BOZORTH_MINUTIAE],
       yvals[MAX_BOZORTH_MINUTIAE],
       tvals[MAX_BOZORTH_MINUTIAE],
       qvals[MAX_BOZORTH_MINUTIAE];

   int i;
   nminutiae = xytq_s->nrows;  
   for (i=0; i<nminutiae; i++)
   {
      xvals_lng[i] = xytq_s->xcol[i];
      yvals_lng[i] = xytq_s->ycol[i];

      if ( xytq_s->thetacol[i] > 180 )
         tvals_lng[i] = xytq_s->thetacol[i] - 360;
      else
         tvals_lng[i] = xytq_s->thetacol[i];

      qvals_lng[i] = xytq_s->qualitycol[i];
   }

   if ( nminutiae > DEFAULT_BOZORTH_MINUTIAE )
   {
      if ( verbose_load )
         fprintf( errorfp, "WARNING: bz_prune(): trimming minutiae to the %d of highest quality\n", DEFAULT_BOZORTH_MINUTIAE );

      if ( verbose_load )
         fprintf( errorfp, "Before quality sort:\n" );
      if ( sort_order_decreasing( qvals_lng, nminutiae, order )) 
      {
         fprintf( errorfp, "ERROR: sort failed and returned on error\n");
         return XYT_NULL;
      }

      for ( j = 0; j < nminutiae; j++ ) 
      {
         if ( verbose_load )
            fprintf( errorfp, "   %3d: %3d %3d %3d ---> order = %3d\n",
                     j, xvals_lng[j], yvals_lng[j], qvals_lng[j], order[j] );

         if ( j == 0 )
            continue;
         if ( qvals_lng[order[j]] > qvals_lng[order[j-1]] ) {
            fprintf( errorfp, "ERROR: sort failed: j=%d; qvals_lng[%d] > qvals_lng[%d]\n", j, order[j], order[j-1] );
            return XYT_NULL;
         }
      }


      if ( verbose_load )
         fprintf( errorfp, "\nAfter quality sort:\n" );
      for ( j = 0; j < DEFAULT_BOZORTH_MINUTIAE; j++ )
      {
         xvals[j] = xvals_lng[order[j]];
         yvals[j] = yvals_lng[order[j]];
         tvals[j] = tvals_lng[order[j]];
         qvals[j] = qvals_lng[order[j]];
         if ( verbose_load )
            fprintf( errorfp, "   %3d: %3d %3d %3d\n", j, xvals[j], yvals[j], qvals[j] );
      }

      if ( verbose_load )
         fprintf( errorfp, "\n" );

      xptr = xvals;
      yptr = yvals;
      tptr = tvals;
      qptr = qvals;

      nminutiae = DEFAULT_BOZORTH_MINUTIAE;
   } 
   else
   {
      xptr = xvals_lng;
      yptr = yvals_lng;
      tptr = tvals_lng;
      qptr = qvals_lng;
   }


   for ( j=0; j < nminutiae; j++ ) 
   {
      c[j].col[0] = xptr[j];
      c[j].col[1] = yptr[j];
      c[j].col[2] = tptr[j];
      c[j].col[3] = qptr[j];
   }
   qsort( (void *) &c, (size_t) nminutiae, sizeof(struct minutiae_struct), sort_x_y );

   if ( verbose_load ) {
      fprintf( errorfp, "\nSorted on increasing x, then increasing y\n" );
      for ( j = 0; j < nminutiae; j++ ) 
      {
         fprintf( errorfp, "%d : %3d, %3d, %3d, %3d\n", j, c[j].col[0], c[j].col[1], c[j].col[2], c[j].col[3] );
         if ( j > 0 ) 
         {
            if ( c[j].col[0] < c[j-1].col[0] ) 
            {
               fprintf( errorfp, "ERROR: sort failed: c[%d].col[0]=%d > c[%d].col[0]=%d\n",
                        j, c[j].col[0], j-1, c[j-1].col[0]
                        );
               return XYT_NULL;
            }
            if ( c[j].col[0] == c[j-1].col[0] && c[j].col[1] < c[j-1].col[1] ) 
            {
               fprintf( errorfp, "ERROR: sort failed: c[%d].col[0]=%d == c[%d].col[0]=%d; c[%d].col[0]=%d == c[%d].col[0]=%d\n",
                        j, c[j].col[0], j-1, c[j-1].col[0],
                        j, c[j].col[1], j-1, c[j-1].col[1]
                        );
               return XYT_NULL;
            }
         }
      }
   }

   xyt_s = (struct xyt_struct *) malloc( sizeof( struct xyt_struct ) );
   if ( xyt_s == XYT_NULL ) 
   {
      fprintf( errorfp, "ERROR: malloc() failure of xyt_struct.");
      return XYT_NULL;
   }

   for ( j = 0; j < nminutiae; j++ ) 
   {
      xyt_s->xcol[j]     = c[j].col[0];
      xyt_s->ycol[j]     = c[j].col[1];
      xyt_s->thetacol[j] = c[j].col[2];
   }
   xyt_s->nrows = nminutiae;

   return xyt_s;
}