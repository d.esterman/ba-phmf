//
// Created by DannyLo on 28.10.2016.
//

#include <bz_client.hpp>

bz_client::bz_client(hpx::future<hpx::id_type> &&gid, const fingerprint_provider& fp)
        : base_type(std::move(gid)),
          fp(fp){
    init();
}

void bz_client::init() {
    string pn_str = Config::GetInstance().GetParameter<string>("PHMF.paillier.paillierPublicKey.n");
    string pg_str = Config::GetInstance().GetParameter<string>("PHMF.paillier.paillierPublicKey.g");
    BigInteger pn(pn_str, 16);
    BigInteger pg(pg_str, 16);
    PaillierPublicKey ppk = {pn, pg};

    string pp_str = Config::GetInstance().GetParameter<string>("PHMF.paillier.paillierPrivateKey.p");
    string pq_str = Config::GetInstance().GetParameter<string>("PHMF.paillier.paillierPrivateKey.q");
    BigInteger pp(pp_str, 16);
    BigInteger pq(pq_str, 16);
    PaillierPrivateKey psk = {pp, pq};

    string dn_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPublicKey.n");
    string dg_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPublicKey.g");
    string dh_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPublicKey.h");
    string du_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPublicKey.u");
    BigInteger dn(dn_str, 16);
    BigInteger dg(dg_str, 16);
    BigInteger dh(dh_str, 16);
    BigInteger du(du_str, 16);
    DgkPublicKey dpk = {dn, dg, dh, du};

    string dp_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPrivateKey.p");
    string dvp_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPrivateKey.vp");
    string dq_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPrivateKey.q");
    string dvq_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPrivateKey.vq");
    BigInteger dp(dp_str, 16);
    BigInteger dq(dq_str, 16);
    BigInteger dvp(dvp_str, 16);
    BigInteger dvq(dvq_str, 16);
    DgkPrivateKey dsk = {dp, dq, dvp, dvq};

    paillier = make_shared<Paillier>(ppk, psk);
    dgk = make_shared<Dgk>(dpk, dsk);
}

bool bz_client::authenticate() {
    const std::string configPath = std::string("SecureFaceRecognition");
    hpx::components::client<hc_comparison_client> scClient =
            hpx::components::local_new<hpx::components::client<hc_comparison_client>>(*paillier, *dgk, configPath);
    hpx::components::client<hc_dgk_comparison_client> dgkClient =
            hpx::components::local_new<hpx::components::client<hc_dgk_comparison_client>>(*paillier, *dgk, configPath);

    std::tuple<const int**, int> t = *fp.get_fingerprint().get();
    const int** p_itafc_ptrs = std::get<0>(t);
    vector<array<PaillierCiphertext, 6>> p_itafc_crypt = encryptItaFC(p_itafc_ptrs, std::get<1>(t));
    typedef bz_server::perform_matching_action action_type;
    vector<PaillierCiphertext> b_values = action_type()(this->get_id(), scClient, dgkClient, p_itafc_crypt);
    for (auto encrypted_b = b_values.begin(); encrypted_b != b_values.end(); ++encrypted_b) {
        BigInteger b = paillier->DecryptInteger(*encrypted_b);
        if (b > THRESHOLD) {
            return true;
        }
    }
    return false;
}

vector<array<PaillierCiphertext, 6>> bz_client::encryptItaFC(const int* const p_itafc_ptrs[], const int &probe_len) {
    vector<array<PaillierCiphertext, 6>> p_itafc_crypt;
    p_itafc_crypt.reserve(probe_len);
    for (int i = 0; i < probe_len; ++i) {
        const int *entry = p_itafc_ptrs[i];
        p_itafc_crypt.emplace_back(
                array<PaillierCiphertext, COLS_SIZE_2> {
                        paillier->EncryptInteger(entry[0]),
                        paillier->EncryptInteger(entry[1]),
                        paillier->EncryptInteger(entry[2]),
                        paillier->EncryptInteger(entry[3]),
                        paillier->EncryptInteger(entry[4]),
                        paillier->EncryptInteger(entry[5])
                }
        );
    }
    return p_itafc_crypt;
}