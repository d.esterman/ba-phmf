//
// Created by DannyLo on 28.10.2016.
//

#ifndef BA_PHMF_BZ3SERVER_HPP
#define BA_PHMF_BZ3SERVER_HPP

// NBIS
#include <hd_bz3.hpp>

// HPX
#include <hpx/hpx.hpp>
#include <hpx/parallel/algorithms/for_loop.hpp>

// SeComLib
#include <paillier.h>
#include <hc_comparison_server.hpp>
#include <hc_comparison_client.hpp>

// STL
#include <vector>

// SOCI
#ifndef __arm__
#include <soci/soci.h>
#endif

// Own headers
#include <export_definitions.hpp>

using namespace SeComLib::Core;
using namespace SeComLib::SecureFaceRecognitionUtils;
using std::vector;
using std::array;
using std::shared_ptr;
using std::make_shared;
#ifndef __arm__
using soci::into;
using soci::use;
#endif

class PHMF_SYMBOL_EXPORT bz_server : public hpx::components::locking_hook<
        hpx::components::component_base<bz_server> > {

#ifndef __arm__
    soci::connection_parameters sql_param;
    soci::session sql;
#endif

    Paillier* paillier;
    Dgk* dgk;

    shared_ptr<hc_comparison_server> sccServer;

    PaillierCiphertext encryptedMinus349;
    PaillierCiphertext encryptedMinus11;
    PaillierCiphertext encryptedMinus1;
    PaillierCiphertext encrypted2;
    PaillierCiphertext encrypted3;
    PaillierCiphertext encrypted11;
    PaillierCiphertext encrypted349;
public:
    bz_server();
    void setCompServer(shared_ptr<hc_comparison_server> sccServer);
    void setPaillier(Paillier* paillier);
    vector<PaillierCiphertext> perform_matching(hpx::components::client<hc_comparison_client> scClient,
                                        hpx::components::client<hc_dgk_comparison_client> dgkClient,
                                        const vector<array<PaillierCiphertext, 6>> &p_itafc_crypt);
    PaillierCiphertext bz_hd_match(const vector<array<PaillierCiphertext, 6>>& p_itafc_crypt,
                                   const vector<array<PaillierCiphertext, 6>>& g_itafc_crypt);

    HPX_DEFINE_COMPONENT_ACTION(bz_server, perform_matching);
};

HPX_REGISTER_ACTION_DECLARATION(
        bz_server::perform_matching_action,
        bz_server);

#endif //BA_PHMF_BZ3SERVER_HPP
