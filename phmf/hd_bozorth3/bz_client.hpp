//
// Created by DannyLo on 28.10.2016.
//

#ifndef BA_PHMF_BZ3CLIENT_HPP
#define BA_PHMF_BZ3CLIENT_HPP

// NBIS
static const int THRESHOLD = 24;

#include <hd_bz3.hpp>
extern "C" {
#include "bz_mindtct_util.h"
};

// HPX
#include <hpx/config.hpp>
#include <hpx/hpx.hpp>

// SeComLib
#include <paillier.h>

// Own headers
#include <export_definitions.hpp>
#include <fingerprint_provider.hpp>
#include <bz_server.hpp>
#include <serialization.hpp>

using namespace SeComLib::Core;
using namespace SeComLib::SecureFaceRecognitionUtils;
using namespace SeComLib::Utils;
using std::vector;
using std::array;
using std::shared_ptr;
using std::make_shared;
using std::string;

class PHMF_SYMBOL_EXPORT bz_client : public hpx::components::client_base<
        bz_client, bz_server> {

    typedef hpx::components::client_base<
            bz_client, bz_server> base_type;

    const fingerprint_provider& fp;

    shared_ptr<Paillier> paillier;
    shared_ptr<Dgk> dgk;

    void init();
public:
    bz_client(hpx::future<hpx::id_type> && gid, const fingerprint_provider& fp);
    bool authenticate();
    vector<array<PaillierCiphertext, 6>> encryptItaFC(const int* const p_itafc_ptrs[], const int& probe_len);
};


#endif //BA_PHMF_BZ3CLIENT_HPP
