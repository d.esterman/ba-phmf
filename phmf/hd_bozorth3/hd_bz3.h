#ifndef _BOZORTH_H
#ifndef _HDBOZORTH_H
#define _HDBOZORTH_H

/* The max number of points in any Probe or Gallery XYT is set to 200; */
/* a pointwise comparison table therefore has a maximum number of:     */
/*		(200^2)/2 = 20000 comparisons. */


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> /* Needed for type pid_t */


/**************************************************************************/
/* Math-Related Macros, Definitions & Prototypes */
/**************************************************************************/
#include <math.h>
				/* This macro adjusts angles to the range (-180,180] */
#define IANGLE180(deg)		( ( (deg) > 180 ) ? ( (deg) - 360 ) : ( (deg) <= -180 ? ( (deg) + 360 ) : (deg) ) )

#define SENSE(a,b)		( (a) < (b) ? (-1) : ( ( (a) == (b) ) ? 0 : 1 ) )
#define SENSE_NEG_POS(a,b)	( (a) < (b) ? (-1) : 1 )

#define SQUARED(n)		( (n) * (n) )

#ifdef M_PI
#define PI		M_PI
#define PI_SINGLE	( (float) PI )
#else
#define PI		3.14159
#define PI_SINGLE	3.14159F
#endif

/**************************************************************************/
/* Array Length Definitions */
/**************************************************************************/
#define COLS_SIZE_2 6
#define SCOLS_SIZE_1 20000
#define FCOLS_SIZE_1 20000

#define SCOLPT_SIZE 20000
#define FCOLPT_SIZE 20000


/**************************************************************************/
/**************************************************************************/
                        /* GENERAL DEFINITIONS */
/**************************************************************************/

#define DEFAULT_BOZORTH_MINUTIAE	150
#define MAX_BOZORTH_MINUTIAE		200

#define DM	125
#define BZ3_FD	5625
#define FDD	500
#define TK	0.05F
#define TXS	121
#define CTXS	121801

/**************************************************************************/
/**************************************************************************/
                         /* STRUCTURES & TYPEDEFS */
/**************************************************************************/

/**************************************************************************/
/* In BZ_SORT.C - supports stdlib qsort() and customized quicksort */
/**************************************************************************/

/* Used by call to stdlib qsort() */
struct minutiae_struct {
	int col[4];
};

/* Used by custom quicksort */
#define BZ_STACKSIZE    1000
struct cell {
	int		index;	/* pointer to an array of pointers to index arrays */
	int		item;	/* pointer to an item array */
};

/**************************************************************************/
/* In BZ_IO : Supports the loading and manipulation of XYT and XYTQ data */
/**************************************************************************/
#define MAX_FILE_MINUTIAE       1000 /* bz_load() */
#define MAX_LINE_LENGTH 1024
#define CNULL  ((char *) NULL)

struct xyt_struct {
	int nrows;
	int xcol[     MAX_BOZORTH_MINUTIAE ];
	int ycol[     MAX_BOZORTH_MINUTIAE ];
	int thetacol[ MAX_BOZORTH_MINUTIAE ];
};

struct xytq_struct {
        int nrows;
        int xcol[     MAX_FILE_MINUTIAE ];
        int ycol[     MAX_FILE_MINUTIAE ];
        int thetacol[ MAX_FILE_MINUTIAE ];
        int qualitycol[ MAX_FILE_MINUTIAE ];
};


#define XYT_NULL ( (struct xyt_struct *) NULL ) /* bz_load() */
#define XYTQ_NULL ( (struct xytq_struct *) NULL ) /* bz_load() */

extern int verbose_bozorth;
extern FILE *errorfp;

/**************************************************************************/
/**************************************************************************/
/* ROUTINE PROTOTYPES */
/**************************************************************************/
/* In: BZ_DRVRS.C */
int bozorth_itafc_init( struct xyt_struct *, int [][ COLS_SIZE_2 ],		/* OUTPUT: pointwise comparison table */
                               int * []	);

/* In: BZ3_CLIENT.C */
void bz_comp(int, int [], int [], int [], int *, int [][COLS_SIZE_2],
                    int *[]);
void bz_find(int *, int *[]);

/* In: BZ_IO.C */
struct xyt_struct *bz_prune(struct xytq_struct *, int);

int sort_order_decreasing(int [], int, int []);
int sort_x_y(const void *, const void *);

#endif /* !_HDBOZORTH_H */
#endif /* !_BOZORTH_H */
