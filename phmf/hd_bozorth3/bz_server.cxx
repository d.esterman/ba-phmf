//
// Created by DannyLo on 28.10.2016.
//

#include <bz_server.hpp>

bz_server::bz_server()
#ifndef __arm__
: sql_param("postgresql", "dbname=postgres user=postgres password=uk4Ofvo83A0u4vxndEa7")
#endif
{

}

vector<PaillierCiphertext> bz_server::perform_matching(hpx::components::client<hc_comparison_client> scClient,
                                               hpx::components::client<hc_dgk_comparison_client> dgkClient,
                                               const vector<array<PaillierCiphertext, 6>> &p_itafc_crypt) {
#ifndef __arm__
    vector<PaillierCiphertext> b_values;

    sql.open(sql_param);

    //Abfragen aller clients
    vector<std::string> client_ids(50);
    sql << "select client_id from ba_phmf.client", into(client_ids);

    //Für jeden Client...
    for (int i = 0; i < client_ids.size(); i++) {

        //Frage seine Keys ab
        std::string pn;
        std::string pg;
        std::string dn;
        std::string dg;
        std::string dh;
        std::string du;
        sql << "select pn, pg, dn, dg, dh, du from ba_phmf.public_keys where client_id = :client_id",
                use(client_ids[i]), into(pn), into(pg), into(dn), into(dg), into(dh), into(du);

        //Erstelle die Kryptoinstanzen
        PaillierPublicKey ppk = {BigInteger(pn, 16), BigInteger(pg, 16)};
        DgkPublicKey dpk = {BigInteger(dn, 16), BigInteger(dg, 16), BigInteger(dh, 16), BigInteger(du, 16)};
        paillier = new Paillier(ppk);
        dgk = new Dgk(dpk);
        shared_ptr<BigInteger> encryptionModulus = make_shared<BigInteger>(paillier->GetEncryptionModulus());

        //Erstelle die Comparison Servers und...
        //Speichere Stubs der Comparison Clients für die spätere Kommunikation mit diesen
        sccServer = make_shared<hc_comparison_server>(*paillier, *dgk, "SecureFaceRecognition");
        sccServer->SetClientStubs(scClient, dgkClient);

        //Frage alle Fingerprints ab, die mit den Schlüsseln des aktuellen Clients verschlüsselt sind
        vector<long> fp_ids(50);
        sql << "select f.fingerprint_id from ba_phmf.fingerprint as f "
                "join ba_phmf.fingerprint_client as fc "
                "on f.fingerprint_id = fc.fingerprint_id "
                "where client_id = :client_id",
                use(client_ids[i]), into(fp_ids);
        for (auto fp_id = fp_ids.begin(); fp_id != fp_ids.end(); ++fp_id) {

            //Frage zu dem aktuellen Fingerprint die Einträge der ItaFC ab
            vector<std::string> d_kj(2000), beta_1(2000), beta_2(2000), k(2000), j(2000), theta_kj(2000);
            sql << "select d_kj, beta_1, beta_2, k, j, theta_kj from ba_phmf.ita_fc_entry where fingerprint_id= :fingerprint_id",
                    use(*fp_id), into(d_kj), into(beta_1), into(beta_2), into(k), into(j), into(theta_kj);
            vector<array<PaillierCiphertext, 6>> g_itafc_crypt;
            g_itafc_crypt.reserve(d_kj.size());
            for (int l = 0; l < d_kj.size(); ++l) {
                g_itafc_crypt.push_back(
                        array<PaillierCiphertext, COLS_SIZE_2> {
                                PaillierCiphertext(BigInteger(d_kj[l], 16), encryptionModulus),
                                PaillierCiphertext(BigInteger(beta_1[l], 16), encryptionModulus),
                                PaillierCiphertext(BigInteger(beta_2[l], 16), encryptionModulus),
                                PaillierCiphertext(BigInteger(k[l], 16), encryptionModulus),
                                PaillierCiphertext(BigInteger(j[l], 16), encryptionModulus),
                                PaillierCiphertext(BigInteger(theta_kj[l], 16), encryptionModulus)
                        }
                );
            }

            //Starte das Matching Verfahren
            b_values.push_back(bz_hd_match(p_itafc_crypt, g_itafc_crypt));
        }
    }

    return b_values;
#else
    std::cout << "Server is not runnable on Raspberry Pi!" << std::endl;
    return vector<PaillierCiphertext>();
#endif
}

void bz_server::setCompServer(shared_ptr<hc_comparison_server> sccServer) {
    this->sccServer = sccServer;
}

void bz_server::setPaillier(Paillier* paillier) {
    this->paillier = paillier;
}

PaillierCiphertext bz_server::bz_hd_match(const vector<array<PaillierCiphertext, 6>> &p_itafc_crypt,
                                          const vector<array<PaillierCiphertext, 6>> &g_itafc_crypt) {

    //Vorbereitung der "Konstanten"
    encryptedMinus349 = paillier->EncryptInteger(-349);
    encryptedMinus11 = paillier->EncryptInteger(-11);
    encryptedMinus1 = -paillier->GetEncryptedOne(0);
    encrypted2 = paillier->EncryptInteger(2);
    encrypted3 = paillier->EncryptInteger(3);
    encrypted11 = paillier->EncryptInteger(11);
    encrypted349 = paillier->EncryptInteger(349);

    PaillierCiphertext b_sum = paillier->GetEncryptedZero(1);

    std::atomic_uint iterations(0);
	std::cout << "Starting a compare with " << std::to_string(p_itafc_crypt.size()) << " probe ita-fc entries versus "
              << g_itafc_crypt.size() << " gallery ita-fc entries."
              << "That's " << std::to_string(p_itafc_crypt.size() * g_itafc_crypt.size()) << " iterations." << std::endl;
    hpx::parallel::for_loop_n(hpx::parallel::par, 0, p_itafc_crypt.size() * g_itafc_crypt.size(),
                              hpx::parallel::reduction_plus(b_sum, paillier->GetEncryptedZero(1)),
                              [&](int i, PaillierCiphertext &b_sum) {
								  unsigned int current_iteration = iterations++;
								  if (current_iteration != 0 && current_iteration % 1000 == 0) {
									  std::cout << std::to_string(current_iteration) << "th iteration, still working..." << std::endl;
								  }

								  
                                  int p = i / g_itafc_crypt.size();
                                  int g = i % g_itafc_crypt.size();
                                  auto p_itafc_entry = p_itafc_crypt[p];
                                  auto g_itafc_entry = g_itafc_crypt[g];
                                  PaillierCiphertext b;
                                  PaillierCiphertext d_kj = (g_itafc_entry[0] - p_itafc_entry[0]) * 10;
                                  PaillierCiphertext t_d = p_itafc_entry[0] + g_itafc_entry[0];

                                  PaillierCiphertext b_1_h1 = sccServer->Compare(encryptedMinus1, d_kj);
                                  PaillierCiphertext b_1_h2 = sccServer->Compare(d_kj, paillier->GetEncryptedZero(0));
                                  PaillierCiphertext b_1_1 = sccServer->Compare(t_d, d_kj);
                                  PaillierCiphertext b_1_2 = sccServer->Compare(d_kj, -t_d);
                                  PaillierCiphertext b_1 = b_1_1 - b_1_h1 + b_1_2 - b_1_h2;
                                  b = b_1;
                                  for (int i = 1; i < 3; i++) {

                                      PaillierCiphertext beta_diff = p_itafc_entry[i] - g_itafc_entry[i];

                                      PaillierCiphertext b_i_h1 = sccServer->Compare(encryptedMinus1,
                                                                                     beta_diff);
                                      PaillierCiphertext b_i_h2 = sccServer->Compare(beta_diff,
                                                                                     paillier->GetEncryptedZero(
                                                                                             0));
                                      PaillierCiphertext b_i_1 = sccServer->Compare(beta_diff, encrypted349);
                                      PaillierCiphertext b_i_2_s = sccServer->Compare(encrypted11, beta_diff);
                                      PaillierCiphertext b_i_3_s = sccServer->Compare(beta_diff,
                                                                                      encryptedMinus11);
                                      PaillierCiphertext b_i_4 = sccServer->Compare(encryptedMinus349,
                                                                                    beta_diff);
                                      PaillierCiphertext b_i = b_i_1 + b_i_2_s - b_i_h1 + b_i_3_s - b_i_h2 + b_i_4;
                                      b = b + b_i; //b_i
                                  }
                                  b = sccServer->Compare(b, encrypted3);

                                  b_sum = b_sum + b;
                              });
    return b_sum;
}

HPX_REGISTER_COMPONENT_MODULE();

HPX_REGISTER_COMPONENT(hpx::components::component<bz_server>, bz_server);

HPX_REGISTER_ACTION(
        hpx::components::component<bz_server>::wrapped_type::perform_matching_action,
        bz_server_perform_matching_action);