//
// Created by DannyLo on 29.10.2016.
//

#ifndef BA_PHMF_BZ3_CLIENT_H
#define BA_PHMF_BZ3_CLIENT_H

#include <lfs.h>

// a routine to convert a bmp-file to a jpg-file
// the passed files must be open
//usage:
/*
    FILE * infile, * outfile;
    infile = fopen("filename.bmp", "rb");
    outfile = fopen("filename.jpg", "wb");
    int exit_code = convert_bmp_to_jpeg(infile, outfile);
    fclose(infile);
    fclose(outfile);
    return exit_code;
*/
int convert_bmp_to_jpeg (FILE * infile, FILE * outfile);

// mindtct from NBIS modified to use locally defined routines and accepting a FILE*
void mindtct(FILE *ifile, int boostflag, MINUTIAE **minutiae, int *ih);

// copied from imgdecod.h but changed the type of the first parameter to FILE*
int _read_and_decode_grayscale_image(FILE *ifile, int *oimg_type,
                                     unsigned char **odata, int *olen,
                                     int *ow, int *oh, int *od);

// copied from imgdecod.h but changed the type of the first parameter to FILE*
int _read_and_decode_image(FILE *ifile, int *oimg_type,
                           unsigned char **odata, int *olen,
                           int *ow, int *oh, int *od, int *oppi, int *ointrlvflag,
                           int *hor_sampfctr, int *vrt_sampfctr, int *on_cmpnts);

// copied from filesize.h but changed the type of the first parameter to FILE*
int _read_raw_from_filesize(FILE *infp, unsigned char **odata, int *ofsize);

// copied from filesize.h but changed the type of the first parameter to FILE*
int _filesize(FILE *fp);

// copied from by_io.c but changed the type of the first parameter to FILE*
struct xyt_struct * _bz_load( FILE * xyt_file );


#endif //BA_PHMF_BZ3_CLIENT_H
