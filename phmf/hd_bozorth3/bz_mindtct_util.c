//
// Created by DannyLo on 28.10.2016.
//

/***********************************************************************/

// сommon
#include <errno.h>
#include <stdio.h>
#include <imgdecod.h>

// for convert_bmp_to_jpeg
#include <stddef.h>
#include <jpeglib.h>
#include <cdjpeg.h>

// for mindtct
#include <imgboost.h>

// for bozorth
#include <hd_bz3.h>

#include <bz_mindtct_util.h>

// a routine to convert a bmp-file to a jpg-file
// the passed files must be open
//usage:
/*
    FILE *infile, *outfile;
    infile = fopen("filename.bmp", "rb");
    outfile = fopen("filename.jpg", "wb");
    int exit_code = convert_bmp_to_jpeg(infile, outfile);
    fclose(infile);
    fclose(outfile);
    return exit_code;
*/
int convert_bmp_to_jpeg (FILE * infile, FILE * outfile)
{
    struct jpeg_compress_struct cinfo;
    struct jpeg_error_mgr jerr;
    cjpeg_source_ptr src_mgr;
    JDIMENSION num_scanlines;
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_compress(&cinfo);
    cinfo.in_color_space = JCS_RGB;
    jpeg_set_defaults(&cinfo);
    cinfo.err->trace_level = 0;
    src_mgr = jinit_read_bmp(&cinfo);
    src_mgr->input_file = infile;
    (*src_mgr->start_input) (&cinfo, src_mgr);
    jpeg_set_colorspace(&cinfo, JCS_GRAYSCALE);
    jpeg_set_quality(&cinfo, 100, TRUE);
    jpeg_stdio_dest(&cinfo, outfile);
    jpeg_start_compress(&cinfo, TRUE);
    while (cinfo.next_scanline < cinfo.image_height) {
        num_scanlines = (*src_mgr->get_pixel_rows) (&cinfo, src_mgr);
        (void) jpeg_write_scanlines(&cinfo, src_mgr->buffer, num_scanlines);
    }
    jpeg_finish_compress(&cinfo);
    jpeg_destroy_compress(&cinfo);
    int exit_code = jerr.num_warnings ? EXIT_WARNING : EXIT_SUCCESS;
    return exit_code;
}


void mindtct(FILE *ifile, int boostflag, MINUTIAE **minutiae, int *ih) {
    unsigned char *idata, *bdata;
    int img_type;
    int ilen, iw, id, bw, bh, bd;
    double ippmm;
    /*int img_idc, img_imp;*/
    int *direction_map, *low_contrast_map, *low_flow_map;
    int *high_curve_map, *quality_map;
    int map_w, map_h;
    int ret;

    /* 1. READ FINGERPRINT IMAGE FROM FILE INTO MEMORY. */

    /* Read the image data from file into memory */
    if ((ret = _read_and_decode_grayscale_image(ifile, &img_type,
                                               &idata, &ilen, &iw, ih, &id))) {
        exit(ret);
    }
    /* If image ppi not defined, then assume 500 */
    ippmm = DEFAULT_PPI / (double) MM_PER_INCH;

    /* 2. ENHANCE IMAGE CONTRAST IF REQUESTED */
    if (boostflag)
        trim_histtails_contrast_boost(idata, iw, *ih);

    /* 3. GET MINUTIAE & BINARIZED IMAGE. */
    if ((ret = get_minutiae(minutiae, &quality_map, &direction_map,
                            &low_contrast_map, &low_flow_map, &high_curve_map,
                            &map_w, &map_h, &bdata, &bw, &bh, &bd,
                            idata, iw, *ih, id, ippmm, &lfsparms_V2))) {
        free(idata);
        exit(ret);
    }

    /* Done with input image data */
    free(idata);

    /* Done with minutiae detection maps. */
    free(quality_map);
    free(direction_map);
    free(low_contrast_map);
    free(low_flow_map);
    free(high_curve_map);

    /* Done with binary image results */
    free(bdata);
}

// copied from imgdecod.c but changed the type of the first parameter to FILE*
int _read_and_decode_grayscale_image(FILE *ifile, int *oimg_type,
                                    unsigned char **odata, int *olen,
                                    int *ow, int *oh, int *od)
{
    int ret;
    unsigned char *idata;
    int img_type, ilen;
    int w, h, d, ppi;
    int intrlvflag;
    int hor_sampfctr[MAX_CMPNTS], vrt_sampfctr[MAX_CMPNTS], n_cmpnts;

    *odata = (unsigned char *)NULL;
    *olen = 0;

    /* Read in and decode image file. */
    if((ret = _read_and_decode_image(ifile, &img_type, &idata, &ilen,
                                    &w, &h, &d, &ppi, &intrlvflag,
                                    hor_sampfctr, vrt_sampfctr, &n_cmpnts))){
        return(ret);
    }

    /* Image type UNKNOWN (perhaps raw), not supported */
    if(img_type == UNKNOWN_IMG){
        free(idata);
        fprintf(stderr, "ERROR : read_and_decode_grayscale_image : ");
        fprintf(stderr, "image type UNKNOWN : not supported\n");
        return(-3);
    }

    /* Only desire grayscale images ... */
    if(d != 8){
        free(idata);
        fprintf(stderr, "ERROR : read_and_decode_grayscale_image : ");
        fprintf(stderr, "image depth : %d != 8\n",
                d);
        return(-4);
    }
    /*}*/

    *oimg_type = img_type;
    *odata = idata;
    *olen = ilen;
    *ow = w;
    *oh = h;
    *od = d;

    return(0);
}

// copied from imgdecod.c but changed the type of the first parameter to FILE*
int _read_and_decode_image(FILE *ifile, int *oimg_type,
                          unsigned char **odata, int *olen,
                          int *ow, int *oh, int *od, int *oppi, int *ointrlvflag,
                          int *hor_sampfctr, int *vrt_sampfctr, int *on_cmpnts)
{
    int ret, i;
    unsigned char *idata, *ndata;
    int img_type, ilen, nlen;
    int w, h, d, ppi, lossyflag, intrlvflag = 0, n_cmpnts;

    if((ret = _read_raw_from_filesize(ifile, &idata, &ilen)))
        return(ret);

    if((ret = image_type(&img_type, idata, ilen))){
        free(idata);
        return(ret);
    }

    switch(img_type){
        case JPEGB_IMG:
            if((ret = jpegb_decode_mem(&ndata, &w, &h, &d, &ppi, &lossyflag,
                                       idata, ilen))){
                free(idata);
                return(ret);
            }
            if(d == 8){
                n_cmpnts = 1;
                intrlvflag = 0;
            }
            else if(d == 24){
                n_cmpnts = 3;
                intrlvflag = 1;
            }
            else{
                fprintf(stderr, "ERROR : read_and_decode_image : ");
                fprintf(stderr, "JPEGB decoder returned d=%d ", d);
                fprintf(stderr, "not equal to 8 or 24\n");
                free(idata);
                return(-2);
            }
            nlen = w * h * (d>>3);
            for(i = 0; i < n_cmpnts; i++){
                hor_sampfctr[i] = 1;
                vrt_sampfctr[i] = 1;
            }
            break;
#ifdef __NBIS_PNG__
        case PNG_IMG:
           if((ret = png_decode_mem(&img_dat, &lossyflag, idata, ilen))){
              free(idata);
              return(ret);
           }
           if((ret = get_IMG_DAT_image(&ndata, &nlen, &w, &h, &d, &ppi,
                                      img_dat))){
              free(idata);
              free_IMG_DAT(img_dat, FREE_IMAGE);
              return(ret);
           }
           /* PNG always returns non-interleaved data. */
           intrlvflag = 0;
           n_cmpnts = img_dat->n_cmpnts;
           if(d == 24){
              for(i = 0; i < n_cmpnts; i++){
                 hor_sampfctr[i] = img_dat->hor_sampfctr[i];
                 vrt_sampfctr[i] = img_dat->vrt_sampfctr[i];
              }
           }
           free_IMG_DAT(img_dat, FREE_IMAGE);
           break;
#endif
        default:
            fprintf(stderr, "ERROR : read_and_decode_image : ");
            fprintf(stderr, "illegal image type = %d\n", img_type);
            return(-3);
    }

    free(idata);

    *oimg_type = img_type;
    *odata = ndata;
    *olen = nlen;
    *ow = w;
    *oh = h;
    *od = d;
    *oppi = ppi;
    *ointrlvflag = intrlvflag;
    *on_cmpnts = n_cmpnts;

    return(0);
}

// copied from imgio.c but changed the type of the first parameter to FILE*
int _read_raw_from_filesize(FILE *infp, unsigned char **odata, int *ofsize)
{
    unsigned char *idata;
    int ret, n, fsize;

    if((ret = _filesize(infp)) < 0)
        return(ret);
    fsize = ret;

    idata = (unsigned char *)malloc(fsize * sizeof(unsigned char));
    if(idata == (unsigned char *)NULL){
        fprintf(stderr, "ERORR : read_raw_from_filesize : malloc : idata\n");
        return(-3);
    }

    n = fread(idata, sizeof(unsigned char), fsize, infp);
    if(n != fsize){
        fprintf(stderr, "ERORR : main : read_raw_from_filesize : ");
        fprintf(stderr, "%d of %d bytes read\n",
                n, fsize);
        return(-4);
    }

    *odata = idata;
    *ofsize = fsize;

    return(0);
}

// copied from filesize.c but changed the type of the first parameter to FILE*
int _filesize(FILE *fp)
{
    int ret;

    /* Move file pointer to end of file. */
    if(fseek(fp, 0, SEEK_END)){
        fprintf(stderr, "ERROR : filesize : seeking to EOF of file failed\n");
        return(-3);
    }

    /* Get byte offest to end of file. */
    if((ret = ftell(fp)) < 0){
        fprintf(stderr, "ERROR : filesize : ftell at EOF failed\n");
        return(-4);
    }

    /* Move file pointer to the beginning of file. */
    rewind(fp);

    /* Return size of file in bytes. */
    return(ret);
}

// copied from by_io.c but changed the type of the first parameter to FILE*
/************************************************************************
Load a 3-4 column (X,Y,T[,Q]) set of minutiae from the specified file
and return a XYT sturcture.
Row 3's value is an angle which is normalized to the interval (-180,180].
A maximum of MAX_BOZORTH_MINUTIAE minutiae can be returned -- fewer if
"max_minutiae" is smaller.  If the file contains more minutiae than are
to be returned, the highest-quality minutiae are returned.
*************************************************************************/

/***********************************************************************/
struct xyt_struct * _bz_load( FILE * xyt_file )
{
    int nminutiae;
    int m;
    int i;
    int nargs_expected;
    struct xyt_struct * xyt_s;
    struct xytq_struct * xytq_s;
    int xvals_lng[MAX_FILE_MINUTIAE],   /* Temporary lists to store all the minutaie from a file */
            yvals_lng[MAX_FILE_MINUTIAE],
            tvals_lng[MAX_FILE_MINUTIAE],
            qvals_lng[MAX_FILE_MINUTIAE];
    char xyt_line[ MAX_LINE_LENGTH ];

    /* This is now externally defined in bozorth.h */
    /* extern FILE * errorfp; */

    nminutiae = 0;
    nargs_expected = 0;

    while ( fgets( xyt_line, sizeof xyt_line, xyt_file ) != CNULL )
    {
        m = sscanf( xyt_line, "%d %d %d %d",
                    &xvals_lng[nminutiae],
                    &yvals_lng[nminutiae],
                    &tvals_lng[nminutiae],
                    &qvals_lng[nminutiae] );

        if ( nminutiae == 0 )
        {
            if ( m != 3 && m != 4 )
            {
                fprintf( errorfp, "ERROR: sscanf() failed on line %u in minutiae file \n",
                         nminutiae+1 );
                return XYT_NULL;
            }
            nargs_expected = m;
        }
        else
        {
            if ( m != nargs_expected )
            {
                fprintf( errorfp, "ERROR: inconsistent argument count on line %u of minutiae file\n",
                         nminutiae+1 );
                return XYT_NULL;
            }
        }
        if ( m == 3 )
            qvals_lng[nminutiae] = 1;

        ++nminutiae;
        if ( nminutiae == MAX_FILE_MINUTIAE )
            break;
    }

    xytq_s = (struct xytq_struct *)malloc(sizeof(struct xytq_struct));
    if ( xytq_s == XYTQ_NULL )
    {
        fprintf( errorfp, "ERROR: malloc() failure while loading minutiae buffer failed: %s\n",
                 strerror(errno)
        );
        return XYT_NULL;
    }

    xytq_s->nrows = nminutiae;
    for (i=0; i<nminutiae; i++)
    {
        xytq_s->xcol[i] = xvals_lng[i];
        xytq_s->ycol[i] = yvals_lng[i];
        xytq_s->thetacol[i] = tvals_lng[i];
        xytq_s->qualitycol[i] = qvals_lng[i];
    }

    xyt_s = bz_prune(xytq_s, 0);

//    if ( verbose_load )
//        fprintf( errorfp, "Loaded %s\n", xyt_file );

    return xyt_s;
}

/***********************************************************************/
void bz_comp(
        int npoints,				/* INPUT: # of points */
        int xcol[     MAX_BOZORTH_MINUTIAE ],	/* INPUT: x cordinates */
        int ycol[     MAX_BOZORTH_MINUTIAE ],	/* INPUT: y cordinates */
        int thetacol[ MAX_BOZORTH_MINUTIAE ],	/* INPUT: theta values */

        int * ncomparisons,			/* OUTPUT: number of pointwise comparisons */
        int cols[][ COLS_SIZE_2 ],		/* OUTPUT: pointwise comparison table */
        int * colptrs[]				/* INPUT and OUTPUT: sorted list of pointers to rows in cols[] */
)
{
    int i, j, k;

    int b;
    int t;
    int n;
    int l;

    int table_index;

    int dx;
    int dy;
    int distance;

    int theta_kj;
    int beta_j;
    int beta_k;

    int * c;



    c = &cols[0][0];

    table_index = 0;
    for ( k = 0; k < npoints - 1; k++ ) {
        for ( j = k + 1; j < npoints; j++ ) {


            if ( thetacol[j] > 0 ) {

                if ( thetacol[k] == thetacol[j] - 180 )
                    continue;
            } else {

                if ( thetacol[k] == thetacol[j] + 180 )
                    continue;
            }


            dx = xcol[j] - xcol[k];
            dy = ycol[j] - ycol[k];
            distance = SQUARED(dx) + SQUARED(dy);
            if ( distance > SQUARED(DM) ) {
                if ( dx > DM )
                    break;
                else
                    continue;

            }

            /* The distance is in the range [ 0, 125^2 ] */
            if ( dx == 0 )
                theta_kj = 90;
            else {
                double dz;

                dz = ( 180.0F / PI_SINGLE ) * atanf( (float) dy / (float) dx );
                if ( dz < 0.0F )
                    dz -= 0.5F;
                else
                    dz += 0.5F;
                theta_kj = (int) dz;
            }


            beta_k = theta_kj - thetacol[k];
            beta_k = IANGLE180(beta_k);

            beta_j = theta_kj - thetacol[j] + 180;
            beta_j = IANGLE180(beta_j);


            if ( beta_k < beta_j ) {
                *c++ = distance;
                *c++ = beta_k;
                *c++ = beta_j;
                *c++ = k+1;
                *c++ = j+1;
                *c++ = theta_kj;
            } else {
                *c++ = distance;
                *c++ = beta_j;
                *c++ = beta_k;
                *c++ = k+1;
                *c++ = j+1;
                *c++ = theta_kj + 400;

            }






            b = 0;
            t = table_index + 1;
            l = 1;
            n = -1;			/* Init binary search state ... */




            while ( t - b > 1 ) {
                int * midpoint;

                l = ( b + t ) / 2;
                midpoint = colptrs[l-1];




                for ( i=0; i < 3; i++ ) {
                    int dd, ff;

                    dd = cols[table_index][i];

                    ff = midpoint[i];


                    n = SENSE(dd,ff);


                    if ( n < 0 ) {
                        t = l;
                        break;
                    }
                    if ( n > 0 ) {
                        b = l;
                        break;
                    }
                }

                if ( n == 0 ) {
                    n = 1;
                    b = l;
                }
            } /* END while */

            if ( n == 1 )
                ++l;




            for ( i = table_index; i >= l; --i )
                colptrs[i] = colptrs[i-1];


            colptrs[l-1] = &cols[table_index][0];
            ++table_index;


            if ( table_index == 19999 ) {
#ifndef NOVERBOSE
                if ( verbose_bozorth )
                    printf( "bz_comp(): breaking loop to avoid table overflow\n" );
#endif
                goto COMP_END;
            }

        } /* END for j */

    } /* END for k */

    COMP_END:
    *ncomparisons = table_index;

}

/***********************************************************************/
void bz_find(
        int * xlim,		/* INPUT:  number of pointwise comparisons in table */
        /* OUTPUT: determined insertion location (NOT ALWAYS SET) */
        int * colpt[]		/* INOUT:  sorted list of pointers to rows in the pointwise comparison table */
)
{
    int midpoint;
    int top;
    int bottom;
    int state;
    int distance;



/* binary search to locate the insertion location of a predefined distance in list of sorted distances */


    bottom   = 0;
    top      = *xlim + 1;
    midpoint = 1;
    state    = -1;

    while ( top - bottom > 1 ) {
        midpoint = ( bottom + top ) / 2;
        distance = *colpt[ midpoint-1 ];
        state = SENSE_NEG_POS(BZ3_FD,distance);
        if ( state < 0 )
            top = midpoint;
        else {
            bottom = midpoint;
        }
    }

    if ( state > -1 )
        ++midpoint;

    if ( midpoint < *xlim )
        *xlim = midpoint;



}