//
// Created by DannyLo on 08.11.2016.
//
#include <gtest/gtest.h>

extern "C" {
#include <bozorth.h>
}

#include <bz_server.hpp>
#include <hpx/hpx_init.hpp>

using SeComLib::Core::Paillier;
using SeComLib::Core::Dgk;

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    hpx::util::function_nonser<int(int, char**)> callback = [](int, char**){ int retval = RUN_ALL_TESTS(); hpx::finalize(); return retval; };
    return hpx::init(callback, argc, argv, hpx::runtime_mode_default);
}

vector<array<PaillierCiphertext, 6>> encryptItaFC(const Paillier& paillier, int* itafc_ptrs[], const int& itafc_len) {
    vector<array<PaillierCiphertext, 6>> itafc_crypt;
    itafc_crypt.reserve(itafc_len);
    for (int i = 0; i < itafc_len; ++i) {
        const int* entry = itafc_ptrs[i];
        itafc_crypt.emplace_back(
                array<PaillierCiphertext, 6> {
                        paillier.EncryptInteger(entry[0]),
                        paillier.EncryptInteger(entry[1]),
                        paillier.EncryptInteger(entry[2]),
                        paillier.EncryptInteger(entry[3]),
                        paillier.EncryptInteger(entry[4]),
                        paillier.EncryptInteger(entry[5])
                }
        );
    }
    return itafc_crypt;
}

TEST(homomorph_bz_test, equality_test) {
    //preparations for bz3
    char program[] = PROGRAM;
    set_progname( 0, program, (pid_t)0 );
    errorfp = stderr;

    //preparations for bz_hd_match
    const std::string configPath = std::string("SecureFaceRecognition");
    verbose_bozorth = 0;
    Paillier clientP; clientP.GenerateKeys();
    Dgk clientD; clientD.GenerateKeys();
    Paillier serverP(clientP.GetPublicKey());
    Dgk serverD(clientD.GetPublicKey());

    bz_server bz3s;
    hpx::components::client<hc_comparison_client> scClient =
            hpx::components::local_new<hpx::components::client<hc_comparison_client>>(clientP, clientD, configPath);
    hpx::components::client<hc_dgk_comparison_client> dgkClient =
            hpx::components::local_new<hpx::components::client<hc_dgk_comparison_client>>(clientP, clientD, configPath);
    const shared_ptr<hc_comparison_client> &cscClient = make_shared<hc_comparison_client>(clientP, clientD, configPath);
    const shared_ptr<hc_comparison_server> &cscServer = make_shared<hc_comparison_server>(serverP, serverD, configPath);
    cscClient->SetServer(cscServer);
    cscServer->SetClientStubs(scClient, dgkClient);
    bz3s.setCompServer(cscServer);
    bz3s.setPaillier(&serverP);

    //read the test xyt-files
    char xyt_file1[] = "test1.xyt";
    char xyt_file2[] = "test2.xyt";
    std::unique_ptr<xyt_struct> xyt1(bz_load(xyt_file1));
    std::unique_ptr<xyt_struct> xyt2(bz_load(xyt_file2));

    if (xyt1 == nullptr || xyt2 == nullptr)
        FAIL();

    //generate itafc
    int probe_len = bozorth_probe_init(xyt1.get());
    int gallery_len = bozorth_gallery_init(xyt2.get());

    //run the original bz_match from bz3
    int score_orig = bz_match(probe_len, gallery_len);

    //encrypt the xyt-structures
    vector<array<PaillierCiphertext, 6>> p_itafc_crypt = encryptItaFC(serverP, scolpt, probe_len);
    vector<array<PaillierCiphertext, 6>> g_itafc_crypt = encryptItaFC(serverP, fcolpt, gallery_len);

    //run the modificated bz_hd_match from sandbox
    PaillierCiphertext score_mod_crypt = bz3s.bz_hd_match(p_itafc_crypt, g_itafc_crypt);
    signed long score_mod = clientP.DecryptInteger(score_mod_crypt).ToSignedLong();

    EXPECT_EQ(score_orig, score_mod);
}


/*
int main(void) {



    return 0;
}*/
