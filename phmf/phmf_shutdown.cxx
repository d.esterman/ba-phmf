//
// Created by DannyLo on 27.10.2016.
//

#include <hpx/hpx_init.hpp>
#include <bz_client.hpp>
#include <file_fingerprint_provider.hpp>
#include <hpx_zfm_fingerprint_provider.hpp>


int main(int argc, char* argv[])
{
    return hpx::init(argc, argv);
}

int hpx_main(int argc, char* argv[]) {
    return hpx::finalize();
}