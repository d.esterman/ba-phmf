//
// Created by DannyLo on 20.05.2017.
//

#include <paillier.h>
#include <dgk.h>
#include <iostream>

using SeComLib::Core::Paillier;
using SeComLib::Core::Dgk;
using std::cout;
using std::endl;

int main() {
    Paillier p;
    Dgk dgk;

    p.GenerateKeys();
    dgk.GenerateKeys();

    cout << "Paillier private key:" << endl;
    cout << "\tp: " << p.GetPrivateKey().p.ToString(16) << endl;
    cout << "\tq: " << p.GetPrivateKey().q.ToString(16) << endl;

    cout << "Paillier public key:" << endl;
    cout << "\tn: " << p.GetPublicKey().n.ToString(16) << endl;
    cout << "\tg: " << p.GetPublicKey().g.ToString(16) << endl;

    cout << "DGK private key:" << endl;
    cout << "\tp: " << dgk.GetPrivateKey().p.ToString(16) << endl;
    cout << "\tvp: " << dgk.GetPrivateKey().vp.ToString(16) << endl;
    cout << "\tq: " << dgk.GetPrivateKey().q.ToString(16) << endl;
    cout << "\tvq: " << dgk.GetPrivateKey().vq.ToString(16) << endl;

    cout << "DGK public key:" << endl;
    cout << "\tn: " << dgk.GetPublicKey().n.ToString(16) << endl;
    cout << "\tg: " << dgk.GetPublicKey().g.ToString(16) << endl;
    cout << "\th: " << dgk.GetPublicKey().h.ToString(16) << endl;
    cout << "\tu: " << dgk.GetPublicKey().u.ToString(16) << endl;

    return 0;
}