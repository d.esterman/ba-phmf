//
// Created by DannyLo on 18.05.2017.
//

#include <hpx/hpx_init.hpp>
#include <hpx_zfm_fingerprint_provider_server.hpp>

int main(int argc, char* argv[])
{
    using boost::program_options::options_description;
    using boost::program_options::value;

    // Configure application-specific options
    options_description
            desc_commandline("Usage: " HPX_APPLICATION_STRING " [options]");

    desc_commandline.add_options()
            ( "count,n"
                    , value<int>()->default_value(1)
                    , "How much fingerprints should be obtained. Default: 1");
    desc_commandline.add_options()
            ( "filename,f"
                    , value<std::string>()->default_value("fingerprint")
                    , "Fingerprint filename prefix. Default: \"fingerprint\"");
    return hpx::init(desc_commandline, argc, argv);
}

int hpx_main(boost::program_options::variables_map& vm) {
    hpx::future<hpx::id_type> f = hpx::agas::on_symbol_namespace_event("phmf_zfm_server", true);
    hpx::components::client<hpx_zfm_fingerprint_provider_server> c = hpx::components::new_<hpx_zfm_fingerprint_provider_server>(f.get());
    int count = vm["count"].as<int>();
    std::string filename = vm["filename"].as<std::string>();
    for (int j = 0; j < count; ++j) {
        hpx::future<std::vector<char>> f = hpx::async<hpx_zfm_fingerprint_provider_server::get_fingerprint_from_zfm20_action>(c);
        std::vector<char> fingerprint = f.get();
        std::ofstream FILE(filename + std::to_string(j) + ".bmp", std::ios::out | std::ofstream::binary);
        std::copy(fingerprint.begin(), fingerprint.end(), std::ostreambuf_iterator<char>(FILE));
        FILE.close();
    }
    hpx::finalize();
}