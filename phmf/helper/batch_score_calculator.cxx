//
// Created by DannyLo on 19.05.2017.
//

#include <hpx/hpx_init.hpp>
#include <hpx/parallel/algorithms/for_loop.hpp>

extern "C" {
#include <bozorth.h>
#include <lfs.h>
#include <bz_mindtct_util.h>
}

int main(int argc, char *argv[]) {
        using boost::program_options::options_description;
    using boost::program_options::value;

    // Configure application-specific options
    options_description
            desc_commandline("Usage: " HPX_APPLICATION_STRING " [options]");

    desc_commandline.add_options()
            ("probedir", value<std::string>()->default_value("probe"),
             "The directory containing the probe fingerprints. Default: \"probe\"");
    desc_commandline.add_options()
            ("probefile", value<std::string>()->default_value("fingerprint"),
             "The prefix for the probe fingerprint files. Default: \"fingerprint\"");
    desc_commandline.add_options()
            ("probecount", value<int>()->default_value(50),
             "The number of probe fingerprints. They should be named suffix0 to suffixn-1. Default: \"50\"");
    desc_commandline.add_options()
            ("gallerydir", value<std::string>()->default_value("gallery"),
             "The directory containing the probe fingerprints. Default: \"gallery\"");
    desc_commandline.add_options()
            ("galleryfile", value<std::string>()->default_value("fingerprint"),
             "The prefix for the gallery fingerprint files. Default: \"fingerprint\"");
    desc_commandline.add_options()
            ("gallerycount", value<int>()->default_value(3),
             "The number of gallery fingerprints. They should be named suffix0 to suffixn-1. Default: \"3\"");
    return hpx::init(desc_commandline, argc, argv);
}

xyt_struct *read_xyt_from_file(std::string filename) {
    FILE *bmpfile;
    if ((bmpfile = fopen(const_cast<char *>(filename.c_str()), "rb")) == (FILE *) NULL) {
        fprintf(stderr, "ERORR : fopen : %s\n", bmpfile);
        return nullptr;
    }
    FILE *jpgfile = tmpfile();
    struct xytq_struct fingerprint_q;
    MINUTIAE *minutiae;
    int ih;
    convert_bmp_to_jpeg(bmpfile, jpgfile);
    mindtct(jpgfile, 0, &minutiae, &ih);
    fclose(jpgfile);
    fclose(bmpfile);

    fingerprint_q.nrows = minutiae->num;
    int i, ox, oy, ot, oq;
    for (i = 0; i < minutiae->num; i++) {
        MINUTIA *minutia = minutiae->list[i];
        lfs2nist_minutia_XYT(&ox, &oy, &ot, minutia, 0 /* not used */, ih);
        oq = sround(minutia->reliability * 100.0);

        fingerprint_q.xcol[i] = ox;
        fingerprint_q.ycol[i] = oy;
        fingerprint_q.thetacol[i] = ot;
        fingerprint_q.qualitycol[i] = oq;
    }

    free_minutiae(minutiae);
    return bz_prune(&fingerprint_q, 0);
}

void write_xyt(std::string path, xyt_struct xyt) {
    std::ofstream FILE(path, std::ios::out | std::ofstream::binary);
    for (int i = 0; i < xyt.nrows; ++i) {
        FILE << std::to_string(xyt.xcol[i]) << " ";
        FILE << std::to_string(xyt.ycol[i]) << " ";
        FILE << std::to_string(xyt.thetacol[i]) << "\n";
    }
    FILE.close();
}

int hpx_main(boost::program_options::variables_map &vm) {
    std::string probedir = vm["probedir"].as<std::string>();
    std::string probefileprefix = vm["probefile"].as<std::string>();
    int probecount = vm["probecount"].as<int>();
    std::string gallerydir = vm["gallerydir"].as<std::string>();
    std::string galleryfileprefix = vm["galleryfile"].as<std::string>();
    int gallerycount = vm["gallerycount"].as<int>();

    boost::filesystem::path probexytdir(probedir + "/xyt");
    boost::filesystem::path galleryxytdir(gallerydir + "/xyt");
    boost::filesystem::create_directory(probexytdir);
    boost::filesystem::create_directory(galleryxytdir);

    std::vector<xyt_struct*> gallery(gallerycount);
    for (int g = 0; g < gallerycount; g++) {
        xyt_struct *gallery_fp =
                read_xyt_from_file(gallerydir + "/" + galleryfileprefix + std::to_string(g) + ".bmp");
        write_xyt(gallerydir + "/xyt/" + galleryfileprefix + std::to_string(g) + ".xyt", *gallery_fp);
        gallery[g] = gallery_fp;
    }

    hpx::parallel::for_loop_n(hpx::parallel::par, 0, probecount, [&](int p) {

        xyt_struct *probe_fp =
                read_xyt_from_file(probedir + "/" + probefileprefix + std::to_string(p) + ".bmp");
        write_xyt(probedir + "/xyt/" + probefileprefix + std::to_string(p) + ".xyt", *probe_fp);


//        int probe_len = bozorth_probe_init(probe_fp);

        std::cout << std::to_string(p) << " ";

        for (int g = 0; g < gallerycount; g++) {
//            int gallery_len = bozorth_gallery_init(gallery[g]);
            int score = bozorth_main(probe_fp, gallery[g]);
            std::cout << score << " ";
        }
        std::cout << std::endl;
    });

    return hpx::finalize();
}