//
// Created by DannyLo on 20.05.2017.
//
#include <hpx/hpx_init.hpp>

#include <hpx_zfm_fingerprint_provider.hpp>
#include <file_fingerprint_provider.hpp>
#include <paillier.h>
#include <soci/soci.h>
#include <serialization.hpp>

using SeComLib::Utils::Config;
using SeComLib::Core::BigInteger;
using SeComLib::Core::Paillier;
using SeComLib::Core::PaillierCiphertext;
using SeComLib::Core::PaillierPublicKey;
using SeComLib::Core::DgkPublicKey;

using std::vector;
using std::array;
using std::tuple;
using std::string;
using std::cout;
using std::endl;

using soci::session;
using soci::use;
using soci::into;

//fwd decl
class Person {
public:
    string first_name;
    string last_name;

    Person(string fn, string ln) : first_name(fn), last_name(ln) {};
};

tuple<PaillierPublicKey, DgkPublicKey> read_keys_from_cfg();

vector<array<PaillierCiphertext, 6>> encryptItaFC(Paillier &p, const int *const p_itafc_ptrs[], const int &probe_len);

void prepare_db(session &sql, Person &p, PaillierPublicKey &pk, DgkPublicKey &dk, long &person_id, vector<int> &client_ids);

int main(int argc, char *argv[]) {
    using boost::program_options::options_description;
    using boost::program_options::value;

    // Configure application-specific options
    options_description
            desc_commandline("Usage: " HPX_APPLICATION_STRING " [options]");

    desc_commandline.add_options()
            ("count,n", value<int>()->default_value(1), "How much fingerprints should be obtained. Default: 1");
    desc_commandline.add_options()
            ("filename,f", value<string>(), "Read fignerprint from file. May be specified multiple times to read from multiple files. Default: 1");
    return hpx::init(desc_commandline, argc, argv);
}

int hpx_main(boost::program_options::variables_map &vm) {
    std::unique_ptr<fingerprint_provider> fp;
    if (vm.count("filename")) {
        string filename = vm["filename"].as<string>();
        fp = std::make_unique<file_fingerprint_provider>(file_fingerprint_provider(filename));
    }
    else {
        //Mit dem ZFM20 am Raspberry verbinden und Fingerprints anfragen
        hpx::future<hpx::id_type> f = hpx::agas::on_symbol_namespace_event("phmf_zfm_server", true);
        fp = std::make_unique<hpx_zfm_fingerprint_provider>(hpx::components::new_<hpx_zfm_fingerprint_provider_server>(f.get()));
    }
    
    //Testdaten
    Person me("Daniel", "Estermann");




    //Schlüssel aus dem Config lesen
    auto keys = read_keys_from_cfg();
    Paillier p(std::get<0>(keys));

    //SQL Verbindung öffnen und die DB vorbereiten.
    session sql("postgresql", "dbname=postgres user=postgres password=uk4Ofvo83A0u4vxndEa7");
    long person_id;
    vector<int> client_ids(5);
    prepare_db(sql, me, std::get<0>(keys), std::get<1>(keys), person_id, client_ids);

    int count = vm["count"].as<int>();
    for (int i = 0; i < count; i++) {
        std::tuple<const int **, int> ita_fc = *fp->get_fingerprint().get();
        const int **p_itafc_ptrs = std::get<0>(ita_fc);

        //Timestamp festhalten
        time_t t = time(0);
        std:
        tm *timestamp = localtime(&t);

        //Den Finerprint verschlüsseln
        vector <array<PaillierCiphertext, 6>> p_itafc_crypt = encryptItaFC(p, p_itafc_ptrs, std::get<1>(ita_fc));

        //Über alle Clients iterieren und den verschlüsselten Fingerabdruck hinzufügen
        for (auto client_id = client_ids.begin(); client_id != client_ids.end(); ++client_id) {
            int fingerprint_id;
            sql << "insert into ba_phmf.fingerprint(timestamp, person_id) "
                    "values(:timestamp, :person_id) returning fingerprint_id",
                    use(*timestamp), use(person_id), into(fingerprint_id);

            for (auto ita_fc_entry = p_itafc_crypt.begin(); ita_fc_entry != p_itafc_crypt.end(); ++ita_fc_entry) {
                sql << "insert into ba_phmf.ita_fc_entry(d_kj, beta_1, beta_2, k, j, theta_kj, fingerprint_id) "
                        "values(:d_kj, :beta_1, :beta_2, :k, :j, :theta_kj, :fingerprint_id)",
                        use((*ita_fc_entry)[0].data.ToString(16)), use((*ita_fc_entry)[1].data.ToString(16)),
                        use((*ita_fc_entry)[2].data.ToString(16)), use((*ita_fc_entry)[3].data.ToString(16)),
                        use((*ita_fc_entry)[4].data.ToString(16)), use((*ita_fc_entry)[5].data.ToString(16)),
                        use(fingerprint_id);
            }

            sql << "insert into ba_phmf.fingerprint_client(fingerprint_id, client_id) "
                    "values(:fingerprint_id, :client_id)",
                    use(fingerprint_id), use(*client_id);
        }
    }
    return hpx::disconnect();
}

void prepare_db(session &sql, Person &p, PaillierPublicKey &pk, DgkPublicKey &dk, long &person_id, vector<int> &client_ids) {
    //Ist die Person schon in der Datenbank?
    sql << "select person_id from ba_phmf.person "
            "where first_name= :fn and last_name= :ln",
            use(p.first_name), use(p.last_name), into(person_id);
    if (person_id == 0) {
        //Falls nein, inserten.
        sql << "insert into ba_phmf.person(first_name, last_name) "
                "values('Daniel', 'Estermann') returning person_id",
                into(person_id);
    }

    //Sind schon irgendwelche Clients angemeldet?
    sql << "select client_id from ba_phmf.client",
            into(client_ids);
    if (client_ids.size() == 0) {
        //Falls nein, den Raspberry Pi mit dem übergebenen Key inserten.

        int client_id;
        sql << "insert into ba_phmf.client(description) "
                "values('Raspberry Pi') returning client_id", into(client_id);
        sql << "insert into ba_phmf.public_keys(pn, pg, dn, dg, dh, du, client_id) "
                "values(:pn, :pg, :dn, :dg, :dh, :du, :client_id)",
                use(pk.n.ToString(16)), use(pk.g.ToString(16)),
                use(dk.n.ToString(16)), use(dk.g.ToString(16)),
                use(dk.h.ToString(16)), use(dk.u.ToString(16)),
                use(client_id);

        client_ids.push_back(client_id);
    }
}

tuple<PaillierPublicKey, DgkPublicKey> read_keys_from_cfg() {
    string pn_str = Config::GetInstance().GetParameter<string>("PHMF.paillier.paillierPublicKey.n");
    string pg_str = Config::GetInstance().GetParameter<string>("PHMF.paillier.paillierPublicKey.g");
    string dn_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPublicKey.n");
    string dg_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPublicKey.g");
    string dh_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPublicKey.h");
    string du_str = Config::GetInstance().GetParameter<string>("PHMF.dgk.dgkPublicKey.u");

    BigInteger pn(pn_str, 16);
    BigInteger pg(pg_str, 16);
    BigInteger dn(dn_str, 16);
    BigInteger dg(dg_str, 16);
    BigInteger dh(dh_str, 16);
    BigInteger du(du_str, 16);

    PaillierPublicKey pk = {pn, pg};
    DgkPublicKey dk = {dn, dg, dh, du};

    return std::make_tuple(pk, dk);
};

vector<array<PaillierCiphertext, 6>>
encryptItaFC(Paillier &paillier, const int *const p_itafc_ptrs[], const int &probe_len) {
    vector<array<PaillierCiphertext, 6>> p_itafc_crypt;
    p_itafc_crypt.reserve(probe_len);
    for (int i = 0; i < probe_len; ++i) {
        const int *entry = p_itafc_ptrs[i];
        p_itafc_crypt.emplace_back(
                array<PaillierCiphertext, COLS_SIZE_2> {
                        paillier.EncryptInteger(entry[0]),
                        paillier.EncryptInteger(entry[1]),
                        paillier.EncryptInteger(entry[2]),
                        paillier.EncryptInteger(entry[3]),
                        paillier.EncryptInteger(entry[4]),
                        paillier.EncryptInteger(entry[5])
                }
        );
    }
    return p_itafc_crypt;
}