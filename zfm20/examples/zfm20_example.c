#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include <wiringSerial.h>
#include "../zfm20/zfm20.h"

int main(int argc, char **argv) {
	if (argc < 2) {
			printf("Usage: main <baud_rate>\n");
			return 1;
	}
	int baudRate = atoi(argv[1]);

    int fd;

    if ((fd  = begin(baudRate)) < 0) {
        fprintf(stderr, "Unable to open serial device: %s\n", strerror(errno));
        return 1;
    }

    char p = -1;

//    char p = setDataPackageLength(fd, 128);
//    switch (p) {
//        case FINGERPRINT_OK:
//            printf("Data package length set to %d\n", 128);
//            break;
//        case FINGERPRINT_PACKETRECIEVEERR:
//            printf("Communication error\n");
//            return 1;
//        case FINGERPRINT_INVALIDREG:
//            printf("Invalid register number\n");
//            return 1;
//        default:
//            printf("Unknown error\n");
//            return 1;
//    }


    if (verifyPassword(fd))
    	printf("Found fingerprint sensor!\n");
    else {
    	printf("Could not find the fingerprint sensor :(\n");
    	return 1;
    }

    //Force STDOUT to be unbufered
    setvbuf(stdout, NULL, _IONBF, 0);

    printf("Waiting for valid finger to enroll\n");
    while (p != FINGERPRINT_OK) {
      p = getImage(fd);
      switch (p) {
      case FINGERPRINT_OK:
        printf("\nImage taken\n");
        break;
      case FINGERPRINT_NOFINGER:
        printf(".");
        break;
      case FINGERPRINT_PACKETRECIEVEERR:
        printf("Communication error\n");
        return 1;
      case FINGERPRINT_IMAGEFAIL:
        printf("Imaging error\n");
        return 1;
      default:
        printf("Unknown error\n");
        return 1;
      }
    }
    printf("\n");

    //Turn the buffer on again
    setvbuf(stdout, NULL, _IOLBF, 0);

    getImageBuffer(fd, "out/fingerPrint1.bmp");

    p = image2Tz(fd, 1);
    switch (p) {
        case FINGERPRINT_OK:
          printf("Image converted\n");
          break;
        case FINGERPRINT_IMAGEMESS:
          printf("Image too messy\n");
          return p;
        case FINGERPRINT_PACKETRECIEVEERR:
          printf("Communication error\n");
          return p;
        case FINGERPRINT_FEATUREFAIL:
          printf("Could not find fingerprint features\n");
          return p;
        case FINGERPRINT_INVALIDIMAGE:
          printf("Could not find fingerprint features\n");
          return p;
        default:
          printf("Unknown error\n");
          return p;
      }

    getModel(fd, 1, "out/fingerPrint1.char");
    switch (p) {
      case FINGERPRINT_OK:
        printf("Characteristics of the first FP transferring\n");
        break;
     default:
        printf("Unknown error %c\n", p);
        return 1;
    }

    //Force STDOUT to be unbufered
    setvbuf(stdout, NULL, _IONBF, 0);

    p = -1;
    printf("Place the same finger again\n");
    while (p != FINGERPRINT_OK) {
      p = getImage(fd);
      switch (p) {
      case FINGERPRINT_OK:
        printf("\nImage taken\n");
        break;
      case FINGERPRINT_NOFINGER:
        printf(".");
        break;
      case FINGERPRINT_PACKETRECIEVEERR:
        printf("Communication error\n");
        return 1;
      case FINGERPRINT_IMAGEFAIL:
        printf("Imaging error\n");
        return 1;
      default:
        printf("Unknown error\n");
        return 1;
      }
    }
    printf("\n");

    //Turn the buffer on again
    setvbuf(stdout, NULL, _IOLBF, 0);

    getImageBuffer(fd, "out/fingerPrint2.bmp");

    p = image2Tz(fd, 2);
    switch (p) {
        case FINGERPRINT_OK:
          printf("Image converted\n");
          break;
        case FINGERPRINT_IMAGEMESS:
          printf("Image too messy\n");
          return p;
        case FINGERPRINT_PACKETRECIEVEERR:
          printf("Communication error\n");
          return p;
        case FINGERPRINT_FEATUREFAIL:
          printf("Could not find fingerprint features\n");
          return p;
        case FINGERPRINT_INVALIDIMAGE:
          printf("Could not find fingerprint features\n");
          return p;
        default:
          printf("Unknown error\n");
          return p;
      }

    getModel(fd, 2, "out/fingerPrint2.char");
    switch (p) {
      case FINGERPRINT_OK:
        printf("Characteristics of the second FP transferring\n");
        break;
     default:
        printf("Unknown error %c\n", p);
        return 1;
    }

    p = createModel(fd);
    if (p == FINGERPRINT_OK) {
      printf("Prints matched!\n");
    } else if (p == FINGERPRINT_PACKETRECIEVEERR) {
      printf("Communication error\n");
      return p;
    } else if (p == FINGERPRINT_ENROLLMISMATCH) {
      printf("Fingerprints did not match\n");
      return p;
    } else {
      printf("Unknown error\n");
      return p;
    }

    getModel(fd, 1, "out/template.char");
    switch (p) {
      case FINGERPRINT_OK:
        printf("Template transferring\n");
        break;
     default:
        printf("Unknown error %c\n", p);
        return 1;
    }


    return 0;
}
