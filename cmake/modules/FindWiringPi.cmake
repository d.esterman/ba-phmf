# Try to find the WiringPi librairies
# WPI_FOUND - system has WiringPi lib
# WPI_INCLUDE_DIR - the WiringPi include directory
# WPI_LIBRARIES - Libraries needed to use WiringPi

if (WPI_INCLUDE_DIR AND WPI_LIBRARY)
    # Already in cache, be silent
    set(WPI_FIND_QUIETLY TRUE)
endif (WPI_INCLUDE_DIR AND WPI_LIBRARY)

find_path(WPI_INCLUDE_DIR NAMES wiringPi.h)
find_library(WPI_LIBRARY NAMES wiringPi)
MESSAGE(STATUS "WiringPi lib: " ${WPI_LIBRARY})

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(WPI DEFAULT_MSG WPI_INCLUDE_DIR WPI_LIBRARY)

mark_as_advanced(WPI_INCLUDE_DIR WPI_LIBRARY)