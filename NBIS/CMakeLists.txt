include(TestBigEndian)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O2 -w -ansi -Wall")

if (CMAKE_HOST_SYSTEM_NAME STREQUAL "Windows")
    add_definitions(-D__MSYS__)
endif ()
add_definitions( -DOPJ_STATIC -D_POSIX_SOURCE -DNO_FORK_AND_EXECL)
TEST_BIG_ENDIAN(BIGENDIAN)
if (NOT ${BIGENDIAN})
    add_definitions(-D__NBISLE__)
endif ()

add_subdirectory(commonnbis)
add_subdirectory(ijg)
add_subdirectory(imgtools)
add_subdirectory(mindtct)
add_subdirectory(bozorth3)
