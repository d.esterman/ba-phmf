set(SOURCE_FILES
        cropcoeff.c
        decoder.c
        encoder.c
        globals.c
        huff.c
        ppi.c
        sd14util.c
        tableio.c
        tree.c
        util.c)

add_library(wsq ${SOURCE_FILES})
target_link_libraries(wsq PRIVATE
        jpegl)