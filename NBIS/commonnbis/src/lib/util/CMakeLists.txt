set(LIB_SOURCE_FILES
        bres.c
        bubble.c
        computil.c
        fatalerr.c
        invbyte.c
        invbytes.c
        memalloc.c
        ssxstats.c
        syserr.c
        #[[ticks.c]]
        time.c)

add_library(nbisutil STATIC ${LIB_SOURCE_FILES})
target_link_libraries(nbisutil PUBLIC
        nbisioutil)