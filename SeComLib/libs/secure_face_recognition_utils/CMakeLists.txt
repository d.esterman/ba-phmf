set(SOURCE_FILES
        src/comparison_blinding_factor_cache_parameters.cpp
        src/dgk_comparison_blinding_factor_cache_parameters.cpp
        src/dgk_comparison_client.cpp
        src/dgk_comparison_server.cpp
        src/secure_comparison_client.cpp
        src/secure_comparison_server.cpp)

add_library(SecureFaceRecognitionUtils
        ${SOURCE_FILES})
target_include_directories(SecureFaceRecognitionUtils PUBLIC
        src)
target_link_libraries(SecureFaceRecognitionUtils
        PUBLIC
        Core)