set(SOURCE_FILES
        src/client.cpp
        src/main.cpp
        src/server.cpp)

add_executable(SecureExtremumSelection
        ${SOURCE_FILES})
target_link_libraries(SecureExtremumSelection PRIVATE
        Utils
        Core
        PrivateRecommendationsUtils
        SecureFaceRecognitionUtils)