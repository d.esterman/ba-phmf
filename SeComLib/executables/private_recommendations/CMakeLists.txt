set(SOURCE_FILES
        src/client.cpp
        src/main.cpp
        src/privacy_service_provider.cpp
        src/service_provider.cpp)

add_executable(PrivateRecommendations
        ${SOURCE_FILES})
target_link_libraries(PrivateRecommendations PRIVATE
        Utils
        Core
        PrivateRecommendationsUtils)