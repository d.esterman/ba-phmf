set(SOURCE_FILES
        src/hub.cpp
        src/main.cpp
        src/secure_svm.cpp
        src/server.cpp)

add_executable(SecureRecommendations
        ${SOURCE_FILES})
target_link_libraries(SecureRecommendations PRIVATE
        Utils
        Core
        SVM)
