CREATE SCHEMA IF NOT EXISTS ba_phmf;

CREATE TABLE IF NOT EXISTS ba_phmf.person (
    person_id bigserial PRIMARY KEY,
    first_name varchar(20) NOT NULL,
    last_name varchar(20) NOT NULL
);


CREATE TABLE IF NOT EXISTS ba_phmf.fingerprint (
    fingerprint_id bigserial PRIMARY KEY,
    timestamp timestamp DEFAULT NULL,
    person_id bigserial REFERENCES ba_phmf.person(person_id)
);

CREATE TABLE IF NOT EXISTS ba_phmf.client (
    client_id bigserial PRIMARY KEY,
    description varchar(200),
    location varchar(200)
);


CREATE TABLE IF NOT EXISTS ba_phmf.public_keys (
    public_keys_id bigserial PRIMARY KEY,
    pn varchar(8192) NOT NULL,
    pg varchar(8192) NOT NULL,
    dn varchar(8192) NOT NULL,
    dg varchar(8192) NOT NULL,
    dh varchar(8192) NOT NULL,
    du varchar(8192) NOT NULL,
    client_id bigserial REFERENCES ba_phmf.client(client_id)
);

CREATE TABLE IF NOT EXISTS ba_phmf.fingerprint_client (
    fingerprint_client bigserial PRIMARY KEY,
    fingerprint_id bigserial REFERENCES ba_phmf.fingerprint(fingerprint_id),
    client_id bigserial REFERENCES ba_phmf.client(client_id)
);

CREATE TABLE IF NOT EXISTS ba_phmf.ita_fc_entry (
    ita_fc_entry_id  bigserial PRIMARY KEY,
    d_kj varchar(8192),
    beta_1 varchar(8192),
    beta_2 varchar(8192),
    k varchar(8192),
    j varchar(8192),
    theta_kj varchar(8192),
    fingerprint_id bigserial REFERENCES ba_phmf.fingerprint(fingerprint_id)
);