DELETE FROM ba_phmf.fingerprint_client;
DELETE FROM ba_phmf.ita_fc_entry;
DELETE FROM ba_phmf.fingerprint_client;
DELETE FROM ba_phmf.fingerprint;
DELETE FROM ba_phmf.public_keys;
DELETE FROM ba_phmf.client;
DELETE FROM ba_phmf.person;

DROP TABLE ba_phmf.ita_fc_entry;
DROP TABLE ba_phmf.fingerprint_client;
DROP TABLE ba_phmf.fingerprint;
DROP TABLE ba_phmf.public_keys;
DROP TABLE ba_phmf.client;
DROP TABLE ba_phmf.person;

DROP SCHEMA ba_phmf;